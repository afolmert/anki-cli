#!/usr/bin/env python
# -*- coding: utf-8 -*-

# cf. https://github.com/navdeep-G/setup.py

# /!\
# Note: To use the 'upload' functionality of this file, you must:
#   $ pipenv install twine --dev
# /!\

import io
import os
import sys
from shutil import rmtree

from setuptools import setup, Command

###############################################################################

# Meta-data:
NAME = 'anki-cli'
DESCRIPTION = 'Anki-cli is a command line interface alternative to Anki.'
URL = 'https://gitlab.com/stephane.tzvetkov/anki-cli'
EMAIL = 'stephane [dot] tzvetkov [at] pm [dot] me'
AUTHOR = 'Stéphane Tzvetkov'
REQUIRES_PYTHON = '>=3.6.0'
VERSION = '0.1.0'

###############################################################################

here = os.path.abspath(os.path.dirname(__file__))

# Import the README and use it as the long-description:
try:
    with io.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary:
about = {}
if not VERSION:
    project_slug = NAME.lower().replace("-", "_").replace(" ", "_")
    with open(os.path.join(here, project_slug, '__version__.py')) as f:
        exec(f.read(), about)
else:
    about['__version__'] = VERSION

###############################################################################


# Define an upload command in order to publish this package to PyPI via Twine:
class UploadCommand(Command):
    """Support setup.py upload."""

    description = 'Build and publish the package.'
    user_options = []

    @staticmethod
    def status(s):
        """Prints things in bold."""
        print('\033[1m{0}\033[0m'.format(s))

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            self.status('Removing previous builds…')
            rmtree(os.path.join(here, 'dist'))
        except OSError:
            pass

        self.status('Building Source and Wheel (universal) distribution…')
        os.system('{0} setup.py sdist bdist_wheel --universal'.format(sys.executable))

        self.status('Uploading the package to PyPI via Twine…')
        os.system('twine upload dist/*')

        self.status('Pushing git tags…')
        os.system('git tag v{0}'.format(about['__version__']))
        os.system('git push --tags')

        sys.exit()

###############################################################################


setup(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=[
        'src',
        'anki-2_1_16-submodule/anki',
        'anki-2_1_16-submodule/anki/importing',
        'anki-2_1_16-submodule/anki/template'
    ],
    entry_points={
        'console_scripts': ['anki-cli=src.anki_cli:main'],
    },
    install_requires=[
        'decorator',
        'requests',
        'send2trash',
        'click',
        'pynput',
    ],
    extras_require={
        'dev': ['mkdocs',
                'mkdocs-material',
                'pylint',
                'pytest',
                'pytest-ordering',
                'pytest-cov',
                'linkchecker',
                'coverage-badge'
                ],
    },
    tests_require=['pytest'],
    include_package_data=True,
    license='MIT',
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    # $ setup.py publish support:
    cmdclass={
        'upload': UploadCommand,
    },
)
