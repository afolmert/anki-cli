
<!--author: anki-cli test user -->
<!--prefix-ids: 2317 -->

# Title

<!--deck

-->

<!--model
{'css': '.card {\n'
        ' font-family: arial;\n'
        ' font-size: 20px;\n'
        ' text-align: center;\n'
        ' color: black;\n'
        ' background-color: white;\n'
        '}\n',
  'did': 1,
  'flds': [{'font': 'Arial',
            'media': [],
            'name': 'Front',
            'ord': 0,
            'rtl': False,
            'size': 20,
            'sticky': False},
           {'font': 'Arial',
            'media': [],
            'name': 'Back',
            'ord': 1,
            'rtl': False,
            'size': 20,
            'sticky': False}],
  'id': '11',
  'latexPost': '\\end{document}',
  'latexPre': '\\documentclass[12pt]{article}\n'
              '\\special{papersize=3in,5in}\n'
              '\\usepackage[utf8]{inputenc}\n'
              '\\usepackage{amssymb,amsmath}\n'
              '\\pagestyle{empty}\n'
              '\\setlength{\\parindent}{0in}\n'
              '\\begin{document}\n',
  'mod': 1585324782,
  'name': 'Demo model',
  'req': [[0, 'any', [0]]],
  'sortf': 0,
  'tags': [],
  'tmpls': [{'afmt': '{{Front}}\n\n<hr id=answer>\n\n{{type:Back}}',
             'bafmt': '',
             'bqfmt': '',
             'did': None,
             'name': 'Card 1',
             'ord': 0,
             'qfmt': '{{Front}}\n\n{{type:Back}}'}],
  'type': 0,
  'usn': 0,
  'vers': []}
-->

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum. 

## Section 1

Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo
pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu
nibh euismod gravida. Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam
tempor. Ut ullamcorper, ligula eu tempor congue, eros est euismod turpis, id tincidunt sapien risus
a quam. Maecenas fermentum consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi. Duis
sapien sem, aliquet nec, commodo eget, consequat quis, neque.

<!--card

-->

<!--note
{"id": "1",
 "mid": "11",
 "tags": ["tag1", "tag2"],
 "flds": ["field1<br>", "field2<br>"],
 "cid": ["32"]}
-->

<!--default-mid 11 -->

<!--auto-card true -->

<!--note {'id': 42, 'tags': ['plop'], 'flds': ['FRONT FIELD<br>', 'BACK FIELD<br>']} -->

<!--note {'id': 42, 'tags': ['plop'], 'flds': [
'FRONT FIELD, cras mollis scelerisque nunc', 'BACK FIELD, Curabitur augue lorem, dapibus quis,
laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci.']} -->

<!--note {'flds': [
'FRONT FIELD, cras mollis scelerisque nunc',
'BACK FIELD, Curabitur augue lorem, dapibus quis, laoreet et, pretium ac, nisi. Aenean magna nisl,
mollis quis, molestie eu, feugiat in, orci.'
]}-->

## Section 2

Cras mollis scelerisque nunc. Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis,
laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci.  

