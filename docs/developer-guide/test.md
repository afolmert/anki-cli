# Test

**WIP**

* Run all the tests (including anki 2.1.16 submodules tests):
```console
$ pytest
```

* Run all anki-cli tests except `sync` related tests:
```console
$ pytest ./tests -m "not sync"
```

* Run only `sync` related tests. In this case, don't forget to provide *your own* AnkiWeb test
  account. So, create a dummy AnkiWeb account for tests purpose, and don't share it with other
  developers in order to avoid tests interferences between two different testers at the same time.
<br/>
<br/>
  After creating a dummy AnkiWeb account, run the `sync` related tests:
  ```console
  $ pytest ./tests -m "sync" --test-id "dummy.anki.test.account@mail.com" --test-pwd "dummy_pwd"
  ```

!!! warning "Warning"
    **Do not put your real AnkiWeb account in the previous test, or it might be wiped out!**

!!! note "Note 1"
    When running all tests with `$ pytest ./tests`, you obviously also run *sync* related tests, so
    you will need to provide the `--test-id` and `--test-pwd options` as well.

!!! note "Note 2"
    Do not run `sync` related tests too often, or AnkiWeb will deny your access for about one hour
    (`Too many logins to your account. Please try again in an hour.`).

* See tests coverage and generate `.coverage` report:
```console
$ pytest --cov=src ./tests
```

* Update code coverage badge (with [coverage-badge](https://pypi.org/project/coverage-badge/)):
```console
$ coverage-badge -o .coverage.svg
```

TODO

## Tests organization

**WIP**

* How are they structured?
* What's the test philosophy of this project?
* Details about the test directory
* Details about the tests' code architecture
