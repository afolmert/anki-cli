
# find-notes

## SYNOPSIS

Todo

## DESCRIPTION

This command will search in your anki collection database (DB) for note(s). In the background,
it will execute a SQL query like `#!MySQL SELECT * FROM notes` against your DB and collect the
results before printing them.

By default, without options, the find-notes command will print all your notes.

## OPTIONS

!!! abstract "-w, --where (TEXT)"
    Extends the SQL query, executed by find-notes in the background,
    with a SQL "WHERE" clause (like `#!MySQL SELECT * FROM notes WHERE ...`).<br />
    E.g. to print all his notes containing to word "foo" among their fields:<br />
    `#!Bash anki-cli find-notes --where "flds LIKE '%foo%'"`.<br />
    **This option cannot be used with --query (-q)!**

!!! abstract "-i, --ids (FLAG)"
    Prints only the found notes' ids, not the full anki-notes objects.<br />
    **This option cannot be used with --query (-q) or with --raw (-r)!**

!!! abstract "-r, --raw (FLAG)"
    Prints the raw DB notes results, not the full anki-notes objects.<br />
    **This option cannot be used with --ids (-i) or with --query (-r)!**

!!! abstract "-q, --query (TEXT)"
    Filters the notes you want to find using the Anki embedded query system
    (cf. https://docs.ankiweb.net/#/searching).<br />
    **This option cannot be used with ANY other option of this command!**

!!! abstract "-h, --help (FLAG)"
    Show this help page and exit.

## EXAMPLES

!!! tip "Search a note with its id:"
    `#!Bash anki-cli find-note -w "id = 42"`

!!! tip "Search notes mismatching some guids:"
    `#!Bash anki-cli find-note -w "guid != q2V and guid != Yvh"`

!!! tip "Search notes with that model id:"
    `#!Bash anki-cli find-note -w "mid = 1585324782719"`

!!! tip "Search notes with that modification time:"
    `#!Bash anki-cli find-note -w "mod = 1585935002"`

!!! tip "Search notes that will be synced:"
    `#!Bash anki-cli find-note -w "usn = -1"`

!!! tip "Search notes with a single tag 'foo':"
    `#!Bash anki-cli find-note -w "tags = foo"`

!!! tip "Search notes with 'foo' in their tags:"
    `#!Bash anki-cli find-note -w "tags like '%foo%'"`

!!! tip "Search notes with 'foo' in their tags:"
    `#!Bash anki-cli find-note -w "instr(tags, 'foo') > 0"`

!!! tip "Search notes with 'foo' in their fields (explicitly case insensitive):"
    `#!Bash anki-cli find-note -w "flds like '%foo%' --case-insensitive"`

!!! tip "Search notes with 'foo' in their fields (implicitly case sensisitive):"
    `$#!Bash anki-cli find-note -f "where instr(flds, 'field content') > 0"`

!!! tip "Search notes with multiple note's attributes:"
    `#!Bash anki-cli find-note -w "tags like '%foo%' or usn = -1 "`<br />
    `#!Bash anki-cli find-note -w "mid = 5 and usn = -1"`<br />
    `#!Bash anki-cli find-note -w "mid >= 6 and mod <= 1585935000"`<br />
    `#!Bash anki-cli find-note -w "mid >= 6 and mod <= 1585935000"`<br />

!!! tip "Search notes and order the results to be printed:"
    `#!Bash anki-cli find-note -w "mid >= 4 order by id DESC"`<br />
    `#!Bash anki-cli find-note -w "mod < 1585935000 order by mid, usn ASC"`<br />

## NOTES

There is a lot of ways to create more complex WHERE clauses. If you want to learn how, you can
start with one of those links:<br />
- https://www.w3schools.com/SQL/default.asp<br />
- https://www.tutorialspoint.com/sql/index.htm<br />
(feel free to propose better references)

So far, the examples given here are rather simplistic. Feel free to propose more practical examples
to improve this section.

