"""
Pytest configuration file.
"""

import pytest

def pytest_addoption(parser):
    """
    Declare custom pytest options (needed for sync testing).
    """
    parser.addoption("--test-id", action="store", default=None, help="Test user id")
    parser.addoption("--test-pwd", action="store", default=None, help="Test user password")

@pytest.fixture
def test_id(request):
    """
    Handle the custom pytest "--test-pwd" option (needed for sync testing).
    """
    return request.config.getoption("--test-id")

@pytest.fixture
def test_pwd(request):
    """
    Handle the custom pytest "--test-id" option (needed for sync testing).
    """
    return request.config.getoption("--test-pwd")
