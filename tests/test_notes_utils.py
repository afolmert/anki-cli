"""
Notes utility functions tests.
"""

import json
import sys
from sys import maxsize as ms # max size
import click
import pytest

import src.notes_utils as nutils
# pylint: disable=W0611 # disable "unused import"
from global_fixtures import *
# pylint: enable=W0611 # enalbe back "unused import"

import src.ankiwrap

# TODO: test with col.db.mod state (instead of src.ankiwrap.modif_should_be_synced state)
# TODO: parametrize all tests that can be
# TODO: make sure that after an error the db won't change
# TODO: test all ids with something like:
#       @pytest.mark.parametrize("bad_id", [-1, 0, sys.maxsize+1, "not an id", None])
# TODO: check for duplicated flds 1 when creating/updating cards, and don't allow empty flds

# =================================================================================================
# Test find_notes(...) function in src/notes_utils.py:
# ====================================================

def check_anki_notes_content(anki_notes, inserted_notes):
    """
    Check Anki-notes content compared to the inserted ones.
    """
    assert len(anki_notes) == len(inserted_notes)
    for note in anki_notes:
        assert note.id in [note['id'] for note in inserted_notes]
        assert note.guid in [note['guid'] for note in inserted_notes]
        assert note.mid in [note['mid'] for note in inserted_notes]
        assert note.mod in [note['mod'] for note in inserted_notes]
        assert note.usn in [note['usn'] for note in inserted_notes]
        assert set(note.tags) in [set(note['tags']) for note in inserted_notes]
        assert set(note.fields) in [set(note['flds']) for note in inserted_notes]
        # cannot assert sfld because Anki dos not store into it's Note object
        # cannot assert csum because Anki dos not store into it's Note object
        assert note.flags in [note['flags'] for note in inserted_notes]
        assert note.data in [note['data'] for note in inserted_notes]

def check_raw_notes_content(raw_notes, inserted_notes):
    """
    Check "raw" notes content compared to the inserted ones.
    """
    assert len(raw_notes) == len(inserted_notes)
    for note in raw_notes:
        assert note[0] in [note['id'] for note in inserted_notes]
        assert note[1] in [note['guid'] for note in inserted_notes]
        assert note[2] in [note['mid'] for note in inserted_notes]
        assert note[3] in [note['mod'] for note in inserted_notes]
        assert note[4] in [note['usn'] for note in inserted_notes]
        # TODO
        #assert note[5] in [set(note['tags']) for note in inserted_notes]
        #assert note[6] in [set(note['flds']) for note in inserted_notes]
        # cannot assert sfld because Anki dos not store into it's Note object
        # cannot assert csum because Anki dos not store into it's Note object
        #assert note[7] in [note['flags'] for note in inserted_notes]
        #assert note[8] in [note['data'] for note in inserted_notes]

def test_find_notes_with_incorrect_col():
    """
    Test find_notes(...) 'col' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'col' argument.
    """
    with pytest.raises(Exception):
        nutils.find_notes(col="not a collection", ids=False, flt=None, raw=False, qry=None)
        nutils.find_notes(col=None, ids=False, flt=None, raw=False, qry=None)

def test_find_notes_with_incorrect_ids(ankiwrap, sql_notes):
    """
    Test find_notes(...) 'ids' argument with wrong type and none.
    This test expects to complete whatever the 'ids' argument.
    """
    anki_notes = nutils.find_notes(col=ankiwrap.col, ids="wrong", flt=None, raw=False, qry=None)
    assert len(anki_notes) == len(sql_notes)
    anki_notes = nutils.find_notes(col=ankiwrap.col, ids=None, flt=None, raw=False, qry=None)
    assert len(anki_notes) == len(sql_notes)

def test_find_notes_with_incorrect_flt(ankiwrap):
    """
    Test find_notes(...) 'flt' argument with wrong type.
    This test expects to catch an exception when using an incorrect 'flt' argument.
    """
    with pytest.raises(click.exceptions.Abort):
        nutils.find_notes(col=ankiwrap.col, ids=False, flt=-9, raw=False, qry=None)

def test_find_notes_with_incorrect_raw(ankiwrap, sql_notes):
    """
    Test find_notes(...) 'raw' argument with wrong type and none.
    This test expects to complete whatever the 'raw' argument.
    """
    anki_notes = nutils.find_notes(col=ankiwrap.col, ids=False, flt=None, raw=-9, qry=None)
    assert len(anki_notes) == len(sql_notes)
    anki_notes = nutils.find_notes(col=ankiwrap.col, ids=False, flt=None, raw=None, qry=None)
    assert len(anki_notes) == len(sql_notes)

def test_find_notes_with_incorrect_qry(ankiwrap):
    """
    Test find_notes(...) 'qry' arguement with wrong type.
    This test expects to catch an exception when using an incorrect 'qry' argument.
    """
    with pytest.raises(click.exceptions.Abort):
        nutils.find_notes(col=ankiwrap.col, ids=False, flt=None, raw=False, qry=-9)

def test_find_notes_with_sql_injection(ankiwrap):
    """
    Test find_notes(...) injecting other sql command.
    This test expects to catch an exception when try to inject sql commands.
    """
    with pytest.raises(click.exceptions.Abort):
        nutils.find_notes(col=ankiwrap.col, ids=False, flt="; DROP ALL TABLES; --", raw=False,
                          qry=None)

def test_find_notes_with_nonexistent_id(ankiwrap):
    """
    Test find_notes(...) with nonexistent id.
    This test expects to find nothing when searching for an nonexistent note id.
    """
    anki_notes = nutils.find_notes(col=ankiwrap.col, ids=False, flt="where id = 0", raw=False,
                                   qry=None)
    assert len(anki_notes) == 0

def test_find_notes_with_all_notes_ids(ankiwrap, sql_notes):
    """
    Test find_notes(...) to find all notes ids.
    This test expects to find all notes' ids.
    """
    anki_notes_ids = nutils.find_notes(col=ankiwrap.col, ids=True, flt=None, raw=False, qry=None)
    assert len(anki_notes_ids) == len(sql_notes)
    for note_id in anki_notes_ids:
        assert note_id in [note['id'] for note in sql_notes]

def test_find_notes_with_default_search(ankiwrap, sql_notes):
    """
    Test find_notes(...) to find all anki-notes with default search.
    This test expects to find all anki-notes with a default search.
    """
    anki_notes = nutils.find_notes(col=ankiwrap.col, ids=False, flt=None, raw=False, qry=None)
    check_anki_notes_content(anki_notes, sql_notes)

def test_find_notes_with_filtered_search(ankiwrap, sql_notes):
    """
    Test find_notes(...) to find all anki-notes with filtered search.
    This test expects to find all anki-notes with an empty filtered search.
    """
    anki_notes = nutils.find_notes(col=ankiwrap.col, ids=False, flt="", raw=False, qry=None)
    check_anki_notes_content(anki_notes, sql_notes)

def test_find_notes_with_raw_search(ankiwrap, sql_notes):
    """
    Test find_notes(...) to find all anki-notes with raw search.
    This test expects to find all anki-notes with a raw search.
    """
    raw_notes = nutils.find_notes(col=ankiwrap.col, ids=False, flt=None, raw=True, qry=None)
    check_raw_notes_content(raw_notes, sql_notes)

def test_find_notes_with_anki_query_search(ankiwrap, sql_notes):
    """
    Test find_notes(...) to find all anki-notes with anki-query search.
    This test expects to find all anki-notes with a query search.
    """
    anki_notes = nutils.find_notes(col=ankiwrap.col, ids=False, flt=None, raw=False, qry="*")
    check_anki_notes_content(anki_notes, sql_notes)

# =================================================================================================
# Test del_notes(...) function in src/notes_utils.py:
# ======================================================

def test_delete_notes_with_incorrect_col():
    """
    Test del_notes(...) 'col' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'col' argument.
    """
    with pytest.raises(Exception):
        nutils.del_notes(col="not a collection", ids=[0])
        nutils.del_notes(col=None, ids=[0])

def test_delete_notes_with_incorrect_ids(ankiwrap):
    """
    Test del_notes(...) 'ids' arg with wrong type, empty list, and none.
    This test expects to catch exceptions when using an incorrect 'ids' argument.
    """
    with pytest.warns(Warning):
        nutils.del_notes(col=ankiwrap.col, ids="not a list")
        nutils.del_notes(col=ankiwrap.col, ids=[])
        nutils.del_notes(col=ankiwrap.col, ids=None)

def test_delete_notes_with_nonexisting_ids(ankiwrap):
    """
    Test del_notes(...) with nonexistent ids.
    This test expects to catch an excetpion when trying to delete notes by passing nonexistent ids.
    """
    with pytest.raises(click.exceptions.Abort):
        nutils.del_notes(col=ankiwrap.col, ids=[0])

def test_delete_notes_with_existing_id(ankiwrap, cursor, sql_notes, sql_card):
    """
    Test delete_note(...) with existing id.
    This test expects to delete a note and its associated cards, thanks to the note id.
    """
    sql_note = sql_notes[0]
    nid = int(sql_note["id"])
    cid = len(sql_notes)
    sql_card(cid=cid+1, nid=nid)
    sql_card(cid=cid+2, nid=nid)
    nutils.del_notes(col=ankiwrap.col, ids=[sql_note['id']])
    cursor.execute(f"select id from notes where id = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 0
    cursor.execute(f"select id from cards where nid = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 0

def test_delete_notes_with_existing_id_without_existing_cards(ankiwrap, cursor, sql_note):
    """
    Test delete_note(...) with existing id but without existing card.
    This test expects to delete a note which has no associated cards.
    """
    sql_note = sql_note(with_card=False)
    nutils.del_notes(col=ankiwrap.col, ids=[sql_note['id']], also_del_cards=False)
    cursor.execute(f"select id from notes where id = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 0

def test_delete_notes_with_existing_id_without_associated_card_to_del(ankiwrap, cursor, sql_note):
    """
    Test delete_note(...) with existing id but without existing card.
    This test expects to delete a note and its associated card, even if it has no associated cards.
    """
    sql_note = sql_note(with_card=False)
    nutils.del_notes(col=ankiwrap.col, ids=[sql_note['id']], also_del_cards=True)
    cursor.execute(f"select id from notes where id = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 0

def test_delete_notes_with_existing_id_without_deleting_cards(ankiwrap, cursor, sql_notes):
    """
    Test del_notes(...) with existing id, but without deleting cards.
    This test expects to delete a note without deleteing it's associated cards.
    """
    sql_note = sql_notes[0]
    with pytest.warns(Warning):
        nutils.del_notes(col=ankiwrap.col, ids=[sql_note['id']], also_del_cards=False)
    cursor.execute(f"select id from notes where id = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 0
    cursor.execute(f"select id from cards where nid = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 1

def test_delete_notes_twice_without_deleting_cards(ankiwrap, cursor, sql_notes):
    """
    Test del_notes(...) with existing id, twice, without deleting cards.
    This test expects catch an exception and a warning when trying to delete a note, twice in the
    same request, without deleteing it's associated cards.
    """
    sql_note = sql_notes[0]
    with pytest.raises(click.exceptions.Abort):
        with pytest.warns(Warning):
            nutils.del_notes(col=ankiwrap.col, ids=[sql_note['id'], sql_note['id']],
                             also_del_cards=False)
    cursor.execute(f"select id from notes where id = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 0
    cursor.execute(f"select id from cards where nid = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 1

def test_delete_notes_with_an_already_deleted_note_id(ankiwrap, cursor, sql_notes):
    """
    Test del_notes(...) with an already deleted note's id.
    This test expects to catch an exception and a warning when trying to delete an already deleted
    note.
    """
    sql_note = sql_notes[0]
    # Delete a first time:
    with pytest.warns(Warning):
        nutils.del_notes(col=ankiwrap.col, ids=[sql_note['id']], also_del_cards=False)
    # Delete a second time and test:
    with pytest.raises(click.exceptions.Abort):
        with pytest.warns(Warning):
            nutils.del_notes(col=ankiwrap.col, ids=[sql_note['id']])
    cursor.execute(f"select id from notes where id = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 0
    # The associated card of an already deleted note should'nt be deleted if it hasn't been deleted
    # in the first place.
    cursor.execute(f"select id from cards where nid = {sql_note['id']}")
    results = cursor.fetchall()
    assert len(results) == 1

# =================================================================================================
# Test set_note_nid(...) function in src/notes_utils.py:
# ======================================================

@pytest.mark.parametrize("new_nid, vrb", [(ms-1, True), (ms-2, "wrong type"), (ms-3, None)])
def test_set_note_nid_with_verbose(ankiwrap, sql_notes, new_nid, vrb):
    """
    Test set_note_nid(...) with an incorrect 'vrb' argument.
    Expect to succeed no matter the vrb argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.set_note_nid(col=ankiwrap.col, note=anki_note, new_nid=new_nid, vrb=vrb)

@pytest.mark.parametrize("bad_col", [42, "not a col", None])
def test_set_note_nid_with_incorrect_col(ankiwrap, sql_notes, bad_col):
    """
    Test set_note_nid(...) an with incorrect 'col' argument.
    This test expects to catch an exception when using an incorrect 'col' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_nid(col=bad_col, note=anki_note, new_nid=999999)

@pytest.mark.parametrize("bad_note", [42, "not a note", None])
def test_set_note_nid_with_incorrect_note(ankiwrap, bad_note):
    """
    Test set_note_nid(...) with an incorrect 'note' argument.
    This test expects to catch an exception when using an incorrect 'note' argument.
    """
    with pytest.raises(Exception):
        nutils.set_note_nid(col=ankiwrap.col, note=bad_note, new_nid=999999)

@pytest.mark.parametrize("bad_nid", [-1, 0, ms+1, "not a nid", None])
def test_set_note_nid_with_incorrect_nid(ankiwrap, sql_notes, bad_nid):
    """
    Test set_note_nid(...) with an incorrect 'new_id' argument.
    This test expects to catch an exception when using an incorrect 'new_id' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_nid(col=ankiwrap.col, note=anki_note, new_nid=bad_nid)

def test_set_note_nid_with_already_existent_nid(ankiwrap, sql_notes):
    """
    Test set_note_nid(...) with already used 'new_id' arguments.
    This test expects to catch a exceptions when trying to set ids that are already used.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        for sql_note in sql_notes:
            existing_nid = sql_note['id']
            nutils.set_note_nid(col=ankiwrap.col, note=anki_note, new_nid=existing_nid)

def test_set_note_nid_with_correct_args(ankiwrap, cursor, sql_notes):
    """
    Test set_note_nid(...) with already used 'new_id' arguments.
    This test expects to set a new nid to a note, making sure there is no note left with old nid.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    old_nid = anki_note.id
    new_nid = ms-1
    nutils.set_note_nid(col=ankiwrap.col, note=anki_note, new_nid=new_nid)
    cursor.execute(f"select id from notes where id = {old_nid};")
    results = cursor.fetchall()
    assert len(results) == 0
    cursor.execute(f"select id from notes where id = {new_nid};")
    results = cursor.fetchall()
    assert len(results) == 1

def test_set_note_nid_with_associated_card(ankiwrap, cursor, sql_notes):
    """
    Test set_note_nid(...) by checking the associated card of the note.
    This test expects to find the associated - unchanged - card of the note .
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    new_nid = ms-1
    nutils.set_note_nid(col=ankiwrap.col, note=anki_note, new_nid=new_nid)
    cursor.execute(f"select * from cards where nid = {new_nid};")
    results = cursor.fetchall()
    assert len(results) == 1


# =================================================================================================
# Test set_note_mid(...) function in src/notes_utils.py:
# ======================================================

def test_set_note_mid_with_verbose(ankiwrap, sql_notes):
    """
    Test set_note_mid(...) 'vrb' argument with correct type, wrong type and none.
    Expect to succeed no matter the vrb argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    # expect to not raise warnings/errors:
    nutils.set_note_mid(col=ankiwrap.col, note=anki_note, mid=4, vrb=True)
    nutils.set_note_mid(col=ankiwrap.col, note=anki_note, mid=4, vrb="wrong type")
    nutils.set_note_mid(col=ankiwrap.col, note=anki_note, mid=4, vrb=None)

def test_set_note_mid_with_incorrect_col(ankiwrap, sql_notes):
    """
    Test set_note_mid(...) 'col' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'col' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_mid(col="not a collection", note=anki_note, mid=4)
        nutils.set_note_mid(col=None, note=anki_note, mid=4)

def test_set_note_mid_with_incorrect_note(ankiwrap):
    """
    Test set_note_mid(...) 'note' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'note' argument.
    """
    with pytest.raises(Exception):
        nutils.set_note_mid(col=ankiwrap.col, note="not a note", mid=4)
        nutils.set_note_mid(col=ankiwrap.col, note=None, mid=4)

def test_set_note_mid_with_incorrect_mid(ankiwrap, sql_notes):
    """
    Test set_note_mid(...) 'mid' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'mid' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_mid(col=ankiwrap.col, note=anki_note, mid="not a mid")
        nutils.set_note_mid(col=ankiwrap.col, note=anki_note, mid=None)

def test_set_note_mid_with_nonexistent_mid(ankiwrap, sql_notes):
    """
    Test set_note_mid(...) with a nonexisting mid.
    This test expects to catch exceptions when trying to set a note's mid with a nonexistent mid.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_mid(col=ankiwrap.col, note=anki_note, mid=999999)
        nutils.set_note_mid(col=ankiwrap.col, note=anki_note, mid=-1)

def test_set_note_mid_with_correct_args(ankiwrap, cursor, sql_notes):
    """
    Test set_note_mid(...) with correct arguments.
    This test expects to set a note's mid with a correct/existing mid.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.set_note_mid(col=ankiwrap.col, note=anki_note, mid=4)
    cursor.execute(f"select mid from notes where id = {anki_note.id}")
    sql_mid = cursor.fetchone()[0]
    assert sql_mid == 4

# =================================================================================================
# Test set_note_model_did(...) function in src/notes_utils.py:
# ============================================================

def test_set_note_model_did_with_verbose(ankiwrap, sql_notes):
    """
    Test set_note_model_did(...) 'vrb' argument with correct type, wrong type and none.
    Expect to succeed no matter the vrb argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    # expect to not raise warnings/errors:
    nutils.set_note_model_did(col=ankiwrap.col, note=anki_note, did=1, vrb=True)
    nutils.set_note_model_did(col=ankiwrap.col, note=anki_note, did=1, vrb="wrong type")
    nutils.set_note_model_did(col=ankiwrap.col, note=anki_note, did=1, vrb=None)

def test_set_note_model_did_with_incorrect_col(ankiwrap, sql_notes):
    """
    Test set_note_model_did(...) 'col' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'col' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_model_did(col="not a collection", note=anki_note, did=1)
        nutils.set_note_model_did(col=None, note=anki_note, did=1)

def test_set_note_model_did_with_incorrect_note():
    """
    Test set_note_model_did(...) 'note' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'note' argument.
    """
    with pytest.raises(Exception):
        nutils.set_note_model_did(col=None, note="not an anki-note", did=1)
        nutils.set_note_model_did(col=None, note=None, did=1)

def test_set_note_model_did_with_incorrect_did(ankiwrap, sql_notes):
    """
    Test set_note_model_did(...) 'did' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'did' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_model_did(col=ankiwrap.col, note=anki_note, did="not a did")
        nutils.set_note_model_did(col=ankiwrap.col, note=anki_note, did=None)

def test_set_note_model_did_with_nonexisting_did(ankiwrap, sql_notes):
    """
    Test set_note_model_did(...) with nonexisting did.
    This test expects to catch exceptions when trying to a note's model did with a nonexisting did.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(click.exceptions.Abort):
        nutils.set_note_model_did(col=ankiwrap.col, note=anki_note, did=-1)

def test_set_note_model_did_with_correct_args(ankiwrap, cursor, sql_notes, sql_decks):
    """
    Test how set_note_model_did(...) set the deck id of a note's model.
    This test expects to change the model's did of a note with a new did.
    """
    sql_did = sql_decks[0] # get did of the first created deck (after "Default" deck with did "1")
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.set_note_model_did(col=ankiwrap.col, note=anki_note, did=sql_did) # changes model's did
    cursor.execute(f"select models from col") # get all models
    models = cursor.fetchone()[0] # extract models (in anki db struct: there is only one 'col' raw)
    models_json = json.loads(models) # load models (models_json is a dict)
    model_did = -1
    for model in models_json:
        mid = int(models_json[model]["id"])
        if mid == anki_note.mid:
            model_did = models_json[model]["did"]
            break
    assert model_did == sql_did

# =================================================================================================
# Test check_flds(...) function in src/notes_utils.py:
# ====================================================

# pylint: disable=W0212 # disable protected-access in order to play with anki_note._fmap

def test_check_flds_with_incorrect_model(ankiwrap, sql_notes):
    """
    Test check_flds(...) 'model' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'model' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.check_flds(model="not a model", note=anki_note)
        nutils.check_flds(model=None, note=anki_note)

def test_check_flds_with_incorrect_note(ankiwrap, sql_notes):
    """
    Test check_flds(...) 'note' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'note' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_model = ankiwrap.col.models.get(anki_note.model()['id']) # ...and get it's model
    with pytest.raises(Exception):
        nutils.check_flds(model=anki_model, note="not an anki-note")
        nutils.check_flds(model=anki_model, note=None)

def test_check_flds_with_more_flds_names_in_note_than_model(ankiwrap, sql_notes):
    """
    Test check_flds(...) with more fields names in the note than in the model.
    This test expects to catch an exception when checking a note with more field names than its
    associated model.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_model = ankiwrap.col.models.get(anki_note.model()['id']) # ...and get it's model
    anki_note._fmap["new_field_name"] = "new_field_model_flds"
    with pytest.raises(click.exceptions.Abort):
        nutils.check_flds(model=anki_model, note=anki_note)
    del anki_note._fmap["new_field_name"]

def test_check_flds_with_more_flds_values_than_flds_names(ankiwrap, sql_notes):
    """
    Test check_flds(...) with more fields values than fields names.
    This test expects to catch an exception when checking a note with more field values than field
    names.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_model = ankiwrap.col.models.get(anki_note.model()['id']) # ...and get it's model
    anki_note.fields.append("new field")
    with pytest.raises(click.exceptions.Abort):
        nutils.check_flds(model=anki_model, note=anki_note)
    del anki_note.fields[-1]

def test_check_flds_with_more_flds_values_in_note_than_model(ankiwrap, sql_notes):
    """
    Test check_flds(...) with more fields values in the note than in the model.
    This test expects to catch an exception when checking a note with more field values than its
    associated model.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_model = ankiwrap.col.models.get(anki_note.model()['id']) # ...and get it's model
    anki_note.fields.append("new field")
    anki_note._fmap["new_field_name"] = "new_field_model_flds"
    with pytest.raises(click.exceptions.Abort):
        nutils.check_flds(model=anki_model, note=anki_note)
    del anki_note._fmap["new_field_name"]
    del anki_note.fields[-1]

def test_check_flds_note_with_the_fields_names_consistency(ankiwrap, sql_notes):
    """
    Test check_flds(...) note's fields names consistency with model fields names.
    This test expects to catch an exception when checking a note that hasn't the same fields names
    than its associated model.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_model = ankiwrap.col.models.get(anki_note.model()['id']) # ...and get it's model
    for field_name in anki_note._fmap:
        last_field_name = field_name
    last_field_value = anki_note._fmap[last_field_name]
    del anki_note._fmap[last_field_name]
    anki_note._fmap["last_field_name"] = "last_field_model_flds"
    with pytest.raises(click.exceptions.Abort):
        nutils.check_flds(model=anki_model, note=anki_note)
    del anki_note._fmap["last_field_name"]
    anki_note._fmap[last_field_name] = last_field_value

def test_check_flds_with_default_note_and_associated_model(ankiwrap, sql_notes):
    """
    Test check_flds(...) with a default test note and it's associated model.
    This test expects to check a note's fields (and associated model) without throwing any warnings
    or exceptions.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_model = ankiwrap.col.models.get(anki_note.model()['id']) # ...and get it's model
    nutils.check_flds(model=anki_model, note=anki_note)

# pylint: enable=W0212 # enable back protected-access

# =================================================================================================
# Test set_note_flds(...) function in src/notes_utils.py:
# =======================================================

def test_set_note_flds_with_verbose(ankiwrap, sql_notes):
    """
    Test set_note_flds(...) 'vrb' argument with correct type, wrong type and none.
    Expect to succeed no matter the vrb argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    # expect to not raise warnings/errors:
    nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=["Q", "A"], vrb=True)
    nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=["Q", "A"], vrb="wrong type")
    nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=["Q", "A"], vrb=None)

def test_set_note_flds_with_silent(ankiwrap, sql_notes):
    """
    Test set_note_flds(...) 'slt' argument with correct type, wrong type and none.
    This test expects to catch warnings (or not) when trying to set no fields to a note.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    # expect to not raise warnings/errors:
    nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=None, slt=True)
    # expect to not raise warnings:
    with pytest.warns(Warning):
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=None, slt=False)

def test_set_note_flds_with_incorrect_col(ankiwrap, sql_notes):
    """
    Test set_note_flds(...) 'col' argument with wrong and none.
    This test expects to catch exceptions when using an incorrect 'col' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_flds(col="wrong type", note=anki_note, flds=["Q", "A"])
        nutils.set_note_flds(col=None, note=anki_note, flds=["Q", "A"])

def test_set_note_flds_with_incorrect_note(ankiwrap):
    """
    Test set_note_flds(...) 'note' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'note' argument.
    """
    with pytest.raises(Exception):
        nutils.set_note_flds(col=ankiwrap.col, note="no note", flds=["Q", "A"])
        nutils.set_note_flds(col=ankiwrap.col, note=None, flds=["Q", "A"])

def test_set_note_flds_with_incorrect_fields_when_needed(ankiwrap, sql_notes):
    """
    Test set_note_flds(...) 'fields' argument with wrong type, empty list and none when needed.
    This test expects to catch exceptions when trying to set no fields in a note.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(click.exceptions.Abort):
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds="empty", flds_are_needed=True)
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=[], flds_are_needed=True)
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=None, flds_are_needed=True)

def test_set_note_flds_with_incorrect_fields_when_not_needed(ankiwrap, sql_notes):
    """
    Test set_note_flds(...) 'fields' argument with empty list and none, when it isn't needed.
    This test expects to catch warnings when trying to set no fields in a note.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.warns(Warning):
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=[], flds_are_needed=False)
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=None, flds_are_needed=False)

def test_set_note_flds_with_too_few_or_too_many_fields(ankiwrap, sql_notes):
    """
    Test set_note_flds(...) with too few or too many field.
    This test expects to catch exceptions when trying to set too many or too few fields in a note
    (compared to its associated model).
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(click.exceptions.Abort):
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=["1"])
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=["1", "2", "3"])

def test_set_note_flds_with_empty_first_field(ankiwrap, sql_notes):
    """
    Test set_note_flds(...) with an empty first field.
    This test expects to catch an exception when trying to set the appropriate number of fields,
    but with the first field being empty.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(click.exceptions.Abort):
        nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=["", "first field is empty"])

def test_set_note_flds_with_correct_fields(ankiwrap, cursor, sql_notes):
    """
    Test set_note_flds(...) with correct fields.
    This test expects to set/replace the fields of a note.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.set_note_flds(col=ankiwrap.col, note=anki_note, flds=["Q", "A"])
    cursor.execute(f"select flds from notes where id = {anki_note.id}")
    flds = cursor.fetchone()[0].split("\x1f")
    assert flds[0] == "Q" and flds[1] == "A"

# =================================================================================================
# Test rip_note_tags(...) function in src/notes_utils.py:
# ========================================================

def test_rip_note_tags_with_verbose(ankiwrap, sql_notes):
    """
    Test rip_note_tags(...) 'vrb' argument with correct type, wrong type and none.
    This test expects to succeed no matter the vrb argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.rip_note_tags(note=anki_note, vrb=True)
    nutils.rip_note_tags(note=anki_note, vrb="wrong type")
    nutils.rip_note_tags(note=anki_note, vrb=None)

def test_rip_note_tags_with_incorrect_note():
    """
    Test rip_note_tags(...) 'note' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'note' argument.
    """
    with pytest.raises(Exception):
        nutils.rip_note_tags(note="not a note")
        nutils.rip_note_tags(note=None)

def test_rip_note_tags_with_correct_args(ankiwrap, cursor, sql_notes, sql_parse_tags):
    """
    Test rip_note_tags(...) with correct 'note' and 'vrb' args.
    This test expects to delete all tags of a note.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.rip_note_tags(note=anki_note, vrb=True)
    cursor.execute(f"select tags from notes where id = {anki_note.id}")
    tags = sql_parse_tags(cursor.fetchone()[0])
    assert len(tags) == 0

# =================================================================================================
# Test set_note_tags(...) function in src/notes_utils.py:
# =======================================================

def test_set_note_tags_with_verbose(ankiwrap, sql_notes):
    """
    Test set_note_tags(...) 'vrb' argument with correct type, wrong type and none.
    This test expects to succeed no matter the vrb argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.set_note_tags(note=anki_note, tags=[], vrb=True)
    nutils.set_note_tags(note=anki_note, tags=[""], vrb=True)
    nutils.set_note_tags(note=anki_note, tags=["test"], vrb=True)
    nutils.set_note_tags(note=anki_note, tags=["te st"], vrb=True)
    nutils.set_note_tags(note=anki_note, tags=["te st", " et ts "], vrb=True)
    nutils.set_note_tags(note=anki_note, tags=["test"], vrb="wrong type")
    nutils.set_note_tags(note=anki_note, tags=["test"], vrb=None)

def test_set_note_tags_with_incorrect_note():
    """
    Test set_note_tags(...) 'note' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'note' argument.
    """
    with pytest.raises(Exception):
        nutils.set_note_tags(note="wrong type", tags=["test"])
        nutils.set_note_tags(note=None, tags=["test"])

def test_set_note_tags_with_incorrect_tags(ankiwrap, sql_notes):
    """
    Test set_note_tags(...) 'tags' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'tags' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.set_note_tags(note=anki_note, tags="wrong type")
        nutils.set_note_tags(note=anki_note, tags=42)

def test_set_note_tags_with_correct_args(ankiwrap, cursor, sql_notes, sql_parse_tags):
    """
    Test set_note_tags(...) with correct 'note', 'tags' and 'vrb' args.
    This test expects to set/replace the tags of a note.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.set_note_tags(note=anki_note, tags=["foo1", "foo2"])
    cursor.execute(f"select tags from notes where id = {anki_note.id}")
    tags = sql_parse_tags(cursor.fetchone()[0])
    assert len(tags) == 2
    assert tags[0] == "foo1"
    assert tags[1] == "foo2"

def test_set_note_tags_with_tags_with_spaces(ankiwrap, cursor, sql_notes, sql_parse_tags):
    """
    Test set_note_tags(...) with spaces in tags.
    This test expects to set a list of tags, each tag containing space separated words: each word
    is expected to become a tag.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.set_note_tags(note=anki_note, tags=[" fo1 o1", "fo2 o2 "])
    cursor.execute(f"select tags from notes where id = {anki_note.id}")
    tags = sql_parse_tags(cursor.fetchone()[0])
    assert len(tags) == 4
    assert tags[0] == "fo1"
    assert tags[1] == "o1"
    assert tags[2] == "fo2"
    assert tags[3] == "o2"

# =================================================================================================
# Test add_note_tags(...) function in src/notes_utils.py:
# =======================================================

def test_add_note_tags_with_verbose(ankiwrap, sql_notes):
    """
    Test add_note_tags(...) 'vrb' argument with correct type, wrong type and none.
    This test expects to succeed no matter the vrb argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.add_note_tags(note=anki_note, tags=[], vrb=True)
    nutils.add_note_tags(note=anki_note, tags=[""], vrb=True)
    nutils.add_note_tags(note=anki_note, tags=["test"], vrb=True)
    nutils.add_note_tags(note=anki_note, tags=["te st"], vrb=True)
    nutils.add_note_tags(note=anki_note, tags=["tset", " ts et "], vrb=True)
    nutils.add_note_tags(note=anki_note, tags=["othertest"], vrb="wrong type")
    nutils.add_note_tags(note=anki_note, tags=["moretest"], vrb=None)

def test_add_note_tags_with_silent(ankiwrap, sql_notes):
    """
    Test add_note_tags(...) 'slt' argument with correct type, wrong type and none.
    This test expects to catch warnings (or not) when trying to add tags that already are in note.
    """
    anki_note1 = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_note2 = ankiwrap.col.getNote(sql_notes[0]['id']) # get the second test note
    # expect to not raise warnings/errors:
    nutils.add_note_tags(note=anki_note1, tags=["duplicated", "duplicated"], slt=True)
    nutils.add_note_tags(note=anki_note1, tags=["not_duplicated"], slt=False)
    # expect to raise warnings:
    with pytest.warns(Warning):
        nutils.add_note_tags(note=anki_note2, tags=["duplicated", "duplicated"], slt=False)
        nutils.add_note_tags(note=anki_note2, tags=["duplicated", "duplicated"], slt=None)
        nutils.add_note_tags(note=anki_note2, tags=["duplicated", "duplicated"], slt="badtype")

def test_add_note_tags_with_incorrect_note():
    """
    Test add_note_tags(...) 'note' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'note' argument.
    """
    with pytest.raises(Exception):
        nutils.add_note_tags(note="wrong type", tags=["test"])
        nutils.add_note_tags(note=None, tags=["test"])

def test_add_note_tags_with_incorrect_tags(ankiwrap, sql_notes):
    """
    Test add_note_tags(...) 'tags' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'tags' argument.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    with pytest.raises(Exception):
        nutils.add_note_tags(note=anki_note, tags="wrong type")
        nutils.add_note_tags(note=anki_note, tags=42)

def test_add_note_tags_with_correct_args(ankiwrap, cursor, sql_notes, sql_parse_tags):
    """
    Test add_note_tags(...) with correct 'note', 'tags' and 'vrb' args.
    This test expects to add a list of tags to a note.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_tags = anki_note.tags.copy()
    new_tags = ["foo1", "foo2"]
    nutils.add_note_tags(note=anki_note, tags=new_tags)
    cursor.execute(f"select tags from notes where id = {anki_note.id}")
    sql_tags = sql_parse_tags(cursor.fetchone()[0])
    assert len(sql_tags) == len(anki_tags + new_tags)
    for tag in sql_tags:
        assert tag in anki_tags + new_tags

def test_add_note_tags_with_tags_with_spaces(ankiwrap, cursor, sql_notes, sql_parse_tags):
    """
    Test add_note_tags(...) with inserted spaces in tags.
    This test expects to add a list of tags, each tag containing space separated words: each word
    is expected to become a tag.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_tags = anki_note.tags.copy()
    new_tags = [" fo o1", "fo o2 "]
    real_new_tags = []
    for tag in new_tags:
        for sub_tag in tag.replace('\u3000', ' ').split(" "):
            if sub_tag:
                real_new_tags.append(sub_tag)
    nutils.add_note_tags(note=anki_note, tags=new_tags)
    cursor.execute(f"select tags from notes where id = {anki_note.id}")
    sql_tags = sql_parse_tags(cursor.fetchone()[0])
    assert len(sql_tags) == len(anki_tags + real_new_tags)
    for tag in sql_tags:
        assert tag in anki_tags + real_new_tags

def test_add_note_tags_with_duplicated_tag(ankiwrap, cursor, sql_notes, sql_parse_tags):
    """
    This test adds an already existing tag expecting this tag to not be added a second time by
    (catching a warning in the process).
    """
    dupe_tag = ["duplicated"]
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.add_note_tags(note=anki_note, tags=dupe_tag) # add the tag a first time
    with pytest.warns(Warning):
        nutils.add_note_tags(note=anki_note, tags=dupe_tag) # add the tag a second time
    cursor.execute(f"select tags from notes where id = {anki_note.id}")
    sql_tags = sql_parse_tags(cursor.fetchone()[0])
    dupe_i = 0
    for tag in sql_tags:
        if tag == dupe_tag[0]:
            dupe_i += 1
    assert dupe_i == 1

# =================================================================================================
# Test del_note_tags(...) function in src/notes_utils.py:
# =======================================================

def test_del_note_tags_with_verbose(ankiwrap, sql_notes):
    """
    Test del_note_tags(...) 'vrb' argument with correct type, wrong type and none.
    This test expects to succeed no matter the vrb argument.
    """
    anki_note1 = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_note2 = ankiwrap.col.getNote(sql_notes[1]['id']) # get the second test note
    # expect to not raise warnings/errors:
    nutils.del_note_tags(note=anki_note1, tags=[anki_note1.tags[1]], vrb=True)
    nutils.del_note_tags(note=anki_note1, tags=[anki_note1.tags[0]], vrb="wrong type")
    nutils.del_note_tags(note=anki_note2, tags=[anki_note2.tags[1]], vrb=None)

def test_del_note_tags_with_silent(ankiwrap, sql_notes):
    """
    Test del_note_tags(...) 'slt' argument with correct type, wrong type and none.
    This test expects to catch warnings (or not) when trying to delete tags that arn't in the note.
    """
    anki_note1 = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    anki_note2 = ankiwrap.col.getNote(sql_notes[0]['id']) # get the second test note
    # expect to not raise warnings/errors:
    nutils.del_note_tags(note=anki_note1, tags=["not_in_note_tags"], slt=True)
    nutils.del_note_tags(note=anki_note1, tags=[anki_note1.tags[1], "bad_tag"], slt=True)
    nutils.del_note_tags(note=anki_note1, tags=[anki_note1.tags[0]], slt=False)
    # expect to raise warnings:
    with pytest.warns(Warning):
        nutils.del_note_tags(note=anki_note2, tags=["not_in_note_tags"], slt=False)
        nutils.del_note_tags(note=anki_note2, tags=[anki_note2.tags[0], "bad_tag"], slt=False)
        nutils.del_note_tags(note=anki_note2, tags=["not_in_note_tags"], slt=None)
        nutils.del_note_tags(note=anki_note2, tags=["not_in_note_tags"], slt="wrong type")

def test_del_note_tags_with_incorrect_note():
    """
    Test del_note_tags(...) 'note' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'note' argument.
    """
    with pytest.raises(Exception):
        nutils.del_note_tags(note="wrong type", tags=["test"])
        nutils.del_note_tags(note=None, tags=["test"])

def test_del_note_tags_with_incorrect_tags():
    """
    Test del_note_tags(...) 'tags' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'tags' argument.
    """
    with pytest.raises(Exception):
        nutils.del_note_tags(note="wrong type", tags=["test"])
        nutils.del_note_tags(note=None, tags=["test"])

def test_del_note_tags_with_correct_args(ankiwrap, cursor, sql_notes, sql_parse_tags):
    """
    Test del_note_tags(...) with correct 'note', 'tags' and 'vrb' args.
    This test expects to delete all tags of one note.
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    nutils.del_note_tags(note=anki_note, tags=anki_note.tags)
    cursor.execute(f"select tags from notes where id = {anki_note.id}")
    sql_tags = sql_parse_tags(cursor.fetchone()[0])
    assert len(sql_tags) == 0

def test_del_note_tags_with_tags_with_spaces(ankiwrap, cursor, sql_notes, sql_parse_tags):
    """
    Test del_note_tags(...) with inserted spaces in tags.
    This test expects to delete all tags of one note, by passing one big tag to del, containing all
    concatenated tags (separated by spaces).
    """
    anki_note = ankiwrap.col.getNote(sql_notes[0]['id']) # get the first test note
    big_tag_to_del = ""
    for tag in anki_note.tags:
        big_tag_to_del += " "+str(tag)
    nutils.del_note_tags(note=anki_note, tags=[big_tag_to_del])
    cursor.execute(f"select tags from notes where id = {anki_note.id}")
    sql_tags = sql_parse_tags(cursor.fetchone()[0])
    assert len(sql_tags) == 0

# =================================================================================================
# Test add_note(...) function in src/notes_utils.py:
# ==================================================

def test_add_note_with_verbose(ankiwrap):
    """
    Test add_note(...) 'vrb' argument with correct type, wrong type and none.
    This test expects to succeed no matter the vrb argument.
    """
    # expect to not raise warnings/errors:
    nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], vrb=True)
    nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], vrb="wrong type")
    nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], vrb=None)

def test_add_note_with_incorrect_col():
    """
    Test add_note(...) 'col' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'col' argument.
    """
    with pytest.raises(Exception):
        nutils.add_note(col="wrong type", flds=["Q", "A"])
        nutils.add_note(col=None, flds=["Q", "A"])

def test_add_note_with_incorrect_flds(ankiwrap):
    """
    Test add_note(...) 'flds' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'flds' argument.
    """
    with pytest.raises(Exception):
        nutils.add_note(col=ankiwrap.col, flds=["", "first fields is empty"])
        nutils.add_note(col=ankiwrap.col, flds="wrong type")
        nutils.add_note(col=ankiwrap.col, flds=None)

def test_add_note_with_incorrect_nid(ankiwrap):
    """
    Test add_note(...) 'nid' argument with wrong type and none.
    This test expects to catch an exception when using a wrong 'nid' argument (but not for none).
    """
    # expect to not raise warnings/errors:
    nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], nid=None)
    # expect to raise errors:
    with pytest.raises(Exception):
        nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], nid="wrong type")
        nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], nid=-1)

def test_add_note_with_incorrect_mid(ankiwrap):
    """
    Test add_note(...) 'mid' argument with wrong type and none.
    This test expects to catch an exception when using a wrong 'mid' argument (but not for none).
    """
    # expect to not raise warnings/errors (because if none, mid will be set to the "Default" mid):
    nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], mid=None)
    # expect to raise errors:
    with pytest.raises(Exception):
        nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], mid="wrong type")

def test_add_note_with_incorrect_did(ankiwrap):
    """
    Test add_note(...) 'did' argument with wrong type and none.
    This test expects to catch an exception when using a wrong 'did' argument (but not for none).
    """
    # expect to not raise warnings/errors:
    nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], did=None)
    # expect to raise errors:
    with pytest.raises(Exception):
        nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], did="wrong type")

def test_add_note_with_incorrect_tags(ankiwrap):
    """
    Test add_note(...) 'tags' argument with wrong type and none.
    This test expects to catch exceptions when using a wrong 'tags' argument (but not for none).
    """
    # expect to not raise warnings/errors:
    nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], tags=None)
    # expect to raise errors:
    with pytest.raises(Exception):
        nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], tags="wrong type")
        nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], tags=42)

def test_add_note_with_correct_flds(ankiwrap, cursor):
    """
    Test add_note(...) with correct 'flds' arg.
    This test expects to set the fields of an added/created note.
    """
    fld0 = "field0"
    fld1 = "field1"
    created_note = nutils.add_note(col=ankiwrap.col, flds=[fld0, fld1])
    cursor.execute(f"select flds from notes where id = {created_note.id};")
    flds = cursor.fetchone()[0].split("\x1f")
    assert flds[0] == fld0
    assert flds[1] == fld1

def test_add_note_with_correct_nid(ankiwrap, cursor):
    """
    Test add_note(...) with correct 'nid' arg.
    This test expects to set the note id of an added/created note.
    """
    created_note = nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], nid=42)
    cursor.execute(f"select id from notes where id = {created_note.id};")
    sql_id = cursor.fetchone()[0]
    assert sql_id == 42

def test_add_note_with_correct_mid(ankiwrap, cursor):
    """
    Test add_note(...) with correct 'mid' arg.
    This test expects to set the model id of an added/created note.
    """
    # e.g. model "4" ("Basic (and reversed card)") instead of default "5" ("Basic"):
    created_note = nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], mid=4)
    cursor.execute(f"select mid from notes where id = {created_note.id};")
    sql_mid = cursor.fetchone()[0]
    assert sql_mid == 4

def test_add_note_with_correct_did(ankiwrap, cursor, sql_decks):
    """
    Test add_note(...) with correct 'did' arg.
    This test expects to set the model's did of an added/created note, with a new did.
    """
    sql_did = sql_decks[0] # get did of the first created deck (after "Default" deck with did "1")
    created_note = nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], did=sql_did) # create note
    cursor.execute(f"select models from col") # get all models
    models = cursor.fetchone()[0] # extract models (in anki db struct: there is only one 'col' raw)
    models_json = json.loads(models) # load modes (models_json is a dict)
    model_did = -1
    for model in models_json:
        mid = models_json[model]["id"]
        if mid == created_note.mid:
            model_did = models_json[model]["did"]
            break
    assert model_did == sql_did

def test_add_note_with_correct_tags(ankiwrap, cursor, sql_parse_tags):
    """
    Test add_note(...) with correct 'tags' arg.
    This test expects to set the tags of an added/created note.
    """
    tags = ["fo o1 ", " fo o2"] # tags with space separated words (expecting each word to be a tag)
    created_note = nutils.add_note(col=ankiwrap.col, flds=["Q", "A"], tags=["fo o1 ", " fo o2"])
    cursor.execute(f"select tags from notes where id = {created_note.id};")
    sql_tags = sql_parse_tags(cursor.fetchone()[0])
    # extract each word as a tag from tags (as expected):
    real_tags = []
    for tag in tags:
        for sub_tag in tag.replace('\u3000', ' ').split(" "):
            if sub_tag:
                real_tags.append(sub_tag)
    # make sure that each retrieved tag is part of the initial tags set (and vice versa)
    for tag in sql_tags:
        assert tag in real_tags
    for tag in real_tags:
        assert tag in sql_tags

def test_add_note_with_associated_card(ankiwrap, cursor):
    """
    Test add_note(...) by checking the associated card of the created note.
    This test expects to find an associated card to the added/created note.
    """
    created_note = nutils.add_note(col=ankiwrap.col, flds=["Q", "A"])
    cursor.execute(f"select id from cards where nid = {created_note.id};")
    results = cursor.fetchall()
    assert len(results) == 1

# =================================================================================================
# Test update_note(...) function in src/notes_utils.py:
# =====================================================

@pytest.mark.parametrize("vrb", [True, "wrong type", None])
def test_upd_note_with_verbose(ankiwrap, sql_notes, vrb):
    """
    Test upd_note(...) 'vrb' argument with correct type, wrong type and none.
    This test expects to succeed no matter the vrb argument.
    """
    nid = sql_notes[0]['id']
    nutils.upd_note(col=ankiwrap.col, nid=nid, vrb=vrb)

@pytest.mark.parametrize("col", ["wrong type", None])
def test_upd_note_with_incorrect_col(sql_notes, col):
    """
    Test upd_note(...) 'col' argument with wrong type and none.
    This test expects to catch exceptions when using an incorrect 'col' argument.
    """
    nid = sql_notes[0]['id']
    with pytest.raises(Exception):
        nutils.upd_note(col=col, nid=nid)

@pytest.mark.parametrize("new_nid", [-42, "wrong type"])
def test_upd_note_with_incorrect_new_nid(ankiwrap, sql_notes, new_nid):
    """
    Test upd_note(...) 'new_nid' argument with a negative model id and wrong type.
    This test expects to catch exceptions when using an incorrect 'new_id' argument.
    """
    nid = sql_notes[0]['id']
    with pytest.raises(Exception):
        nutils.upd_note(col=ankiwrap.col, nid=nid, new_nid=new_nid)

@pytest.mark.parametrize("mid", [999999, -42, "wrong type"])
def test_upd_note_with_incorrect_mid(ankiwrap, sql_notes, mid):
    """
    Test upd_note(...) 'mid' argument with an unused model id, a negative mid and wrong type.
    This test expects to catch exceptions when using an incorrect 'mid' argument.
    """
    nid = sql_notes[0]['id']
    with pytest.raises(Exception):
        nutils.upd_note(col=ankiwrap.col, nid=nid, mid=mid)

@pytest.mark.parametrize("did", [999999, -42, "wrong type"])
def test_upd_note_with_incorrect_did(ankiwrap, sql_notes, did):
    """
    Test upd_note(...) 'did' argument with an unused model id, a negative mid and wrong type.
    This test expects to catch exceptions when using an incorrect 'did' argument.
    """
    nid = sql_notes[0]['id']
    with pytest.raises(Exception):
        nutils.upd_note(col=ankiwrap.col, nid=nid, did=did)

@pytest.mark.parametrize("tags", [42, "wrong type"])
def test_upd_note_with_incorrect_tags(ankiwrap, sql_notes, tags):
    """
    Test upd_note(...) 'tags' argument with  wrong types.
    This test expects to catch exceptions when using an incorrect 'tags' argument.
    """
    nid = sql_notes[0]['id']
    with pytest.raises(Exception):
        nutils.upd_note(col=ankiwrap.col, nid=nid, tags=tags)

@pytest.mark.parametrize("flds", [42, "wrong type"])
def test_upd_note_with_incorrect_flds(ankiwrap, sql_notes, flds):
    """
    Test upd_note(...) 'flds' argument with  wrong types.
    This test expects to catch exceptions when using an incorrect 'flds' argument.
    """
    nid = sql_notes[0]['id']
    with pytest.raises(Exception):
        nutils.upd_note(col=ankiwrap.col, nid=nid, flds=flds)

@pytest.mark.parametrize("new_nid", [999, ms])
def test_upd_note_with_correct_new_nid(ankiwrap, sql_notes, cursor, new_nid):
    """
    Test add_note(...) with correct 'new_nid' arg.
    This test expects to change a note's id.
    """
    nid = sql_notes[0]['id']
    nutils.upd_note(col=ankiwrap.col, nid=nid, new_nid=new_nid)
    cursor.execute(f"select id from notes where id = {new_nid};")
    results = cursor.fetchall()
    assert len(results) == 1

@pytest.mark.parametrize("mid", [4])
def test_upd_note_with_correct_mid(ankiwrap, sql_notes, cursor, mid):
    """
    Test add_note(...) with correct 'mid' arg.
    This test expects to change a note's mid.
    """
    nid = sql_notes[0]['id']
    nutils.upd_note(col=ankiwrap.col, nid=nid, mid=mid)
    cursor.execute(f"select mid from notes where id = {nid};")
    sql_mid = cursor.fetchone()[0]
    assert sql_mid == mid

def test_upd_note_with_correct_did(ankiwrap, sql_notes, cursor, sql_decks):
    """
    Test add_note(...) with correct 'did' arg.
    This test expects to change a note's model did.
    """
    nid = sql_notes[0]['id']
    sql_did = sql_decks[0] # get did of the first created deck (after "Default" deck with did "1")
    upd_note = nutils.upd_note(col=ankiwrap.col, nid=nid, did=sql_did) # update note's model did
    cursor.execute(f"select models from col") # get all models
    models = cursor.fetchone()[0] # extract models (in anki db struct: there is only one 'col' raw)

    models_json = json.loads(models) # load modes (models_json is a dict)
    model_did = -1
    for model in models_json:
        mid = int(models_json[model]["id"])
        if mid == upd_note.mid:
            model_did = models_json[model]["did"]
            break
    assert model_did == sql_did

def test_upd_note_with_correct_tags(ankiwrap, sql_notes, cursor, sql_parse_tags):
    """
    Test upd_note(...) with correct 'tags' arg.
    This test expects to change/set the tags of a note.
    """
    nid = sql_notes[0]['id']
    tags = ["fo o1 ", " fo o2"] # tags with space separated words (expecting each word to be a tag)
    upd_note = nutils.upd_note(col=ankiwrap.col, nid=nid, tags=tags)
    cursor.execute(f"select tags from notes where id = {upd_note.id};")
    sql_tags = sql_parse_tags(cursor.fetchone()[0])
    # extract each word as a tag from tags (as expected):
    real_tags = []
    for tag in tags:
        for sub_tag in tag.replace('\u3000', ' ').split(" "):
            if sub_tag:
                real_tags.append(sub_tag)
    # make sure that each retrieved tag is part of the initial tags set (and vice versa)
    for tag in sql_tags:
        assert tag in real_tags
    for tag in real_tags:
        assert tag in sql_tags

def test_upd_note_with_replaced_tags(ankiwrap, sql_notes, cursor, sql_parse_tags):
    """
    Test upd_note(...) with correct 'tags' arg.
    This test expects to replace the tags of a note.
    """
    nid = sql_notes[0]['id']
    tags = ["fo o1 ", " fo o2"] # tags with space separated words (expecting each word to be a tag)
    upd_note = nutils.upd_note(col=ankiwrap.col, nid=nid, tags=tags)
    replaced_tags = ["pl op ", " cl ap"] # new tags supposed to replace the previous ones
    upd_note = nutils.upd_note(col=ankiwrap.col, nid=nid, tags=replaced_tags)
    cursor.execute(f"select tags from notes where id = {upd_note.id};")
    sql_tags = sql_parse_tags(cursor.fetchone()[0])
    # extract each word as a tag from replaced_tags (as expected):
    real_tags = []
    for tag in replaced_tags:
        for sub_tag in tag.replace('\u3000', ' ').split(" "):
            if sub_tag:
                real_tags.append(sub_tag)
    # make sure that each retrieved tag is part of the initial replaced_tags set (and vice versa)
    for tag in sql_tags:
        assert tag in real_tags
    for tag in real_tags:
        assert tag in sql_tags

def test_upd_note_with_correct_flds(ankiwrap, sql_notes, cursor):
    """
    Test upd_note(...) with correct 'flds' arg.
    This test expects to update the fields of a note.
    """
    nid = sql_notes[0]['id']
    flds = ["foo1", "foo2"]
    upd_note = nutils.upd_note(col=ankiwrap.col, nid=nid, flds=flds)
    cursor.execute(f"select flds from notes where id = {upd_note.id};")
    sql_flds = cursor.fetchone()[0].split("\x1f")
    assert sql_flds[0] == flds[0] and sql_flds[1] == flds[1]

def test_upd_note_with_associated_card(ankiwrap, sql_notes, cursor):
    """
    Test upd_note(...) by checking the associated card of the updated note.
    This test expects to find the associated - unchanged - card of the updated note.
    """
    nid = sql_notes[0]['id'] # by default sql_notes notes comes with associated cards, one per note
    flds = ["foo1", "foo2"]
    upd_note = nutils.upd_note(col=ankiwrap.col, nid=nid, new_nid=42, flds=flds)
    cursor.execute(f"select * from cards where nid = {upd_note.id};")
    results = cursor.fetchall()
    assert len(results) == 1

def test_upd_note_with_duplicated_new_nid(ankiwrap, sql_notes):
    """
    Test add_note(...) with a duplicated 'new_nid' arg.
    This test expects to raise an exception when updating a note with an already existing id.
    """
    nid = sql_notes[0]['id']
    duplicated_nid = sql_notes[1]['id']
    with pytest.raises(Exception):
        nutils.upd_note(col=ankiwrap.col, nid=nid, new_nid=duplicated_nid)
