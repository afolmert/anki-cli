"""
Cards utility functions tests.
"""

import click
import pytest

import src.cards_utils as cutils
# pylint: disable=W0611 # disable "unused import"
from global_fixtures import *
# pylint: enable=W0611 # enalbe back "unused import"

def check_anki_cards_content(anki_cards, inserted_cards):
    """ Check Anki-cards content compared to the inserted ones. """

    assert len(anki_cards) == len(inserted_cards)
    for card in anki_cards:
        assert card.id in [card['id'] for card in inserted_cards]
        assert card.nid in [card['nid'] for card in inserted_cards]
        assert card.did in [card['did'] for card in inserted_cards]
        assert card.ord in [card['ord'] for card in inserted_cards]
        assert card.mod in [card['mod'] for card in inserted_cards]
        assert card.usn in [card['usn'] for card in inserted_cards]
        assert card.type in [card['type'] for card in inserted_cards]
        assert card.queue in [card['queue'] for card in inserted_cards]
        assert card.due in [card['due'] for card in inserted_cards]
        assert card.ivl in [card['ivl'] for card in inserted_cards]
        assert card.factor in [card['factor'] for card in inserted_cards]
        assert card.reps in [card['reps'] for card in inserted_cards]
        assert card.lapses in [card['lapses'] for card in inserted_cards]
        assert card.left in [card['left'] for card in inserted_cards]
        assert card.odue in [card['odue'] for card in inserted_cards]
        assert card.odid in [card['odid'] for card in inserted_cards]
        assert card.flags in [card['flags'] for card in inserted_cards]
        assert card.data in [card['data'] for card in inserted_cards]

def check_raw_cards_content(raw_cards, inserted_cards):
    """ Check "raw" cards content compared to the inserted ones. """

    assert len(raw_cards) == len(inserted_cards)
    for raw_card in raw_cards:
        assert raw_card[0] in [card['id'] for card in inserted_cards]
        assert raw_card[1] in [card['nid'] for card in inserted_cards]
        assert raw_card[2] in [card['did'] for card in inserted_cards]
        assert raw_card[3] in [card['ord'] for card in inserted_cards]
        assert raw_card[4] in [card['mod'] for card in inserted_cards]
        assert raw_card[5] in [card['usn'] for card in inserted_cards]
        assert raw_card[6] in [card['type'] for card in inserted_cards]
        assert raw_card[7] in [card['queue'] for card in inserted_cards]
        assert raw_card[8] in [card['due'] for card in inserted_cards]
        assert raw_card[9] in [card['ivl'] for card in inserted_cards]
        assert raw_card[10] in [card['factor'] for card in inserted_cards]
        assert raw_card[11] in [card['reps'] for card in inserted_cards]
        assert raw_card[12] in [card['lapses'] for card in inserted_cards]
        assert raw_card[13] in [card['left'] for card in inserted_cards]
        assert raw_card[14] in [card['odue'] for card in inserted_cards]
        assert raw_card[15] in [card['odid'] for card in inserted_cards]
        assert raw_card[16] in [card['flags'] for card in inserted_cards]
        assert raw_card[17] in [card['data'] for card in inserted_cards]

# TODO
@pytest.mark.usefixtures("ankiwrap")
@pytest.mark.usefixtures("sql_cards")
def find_cards(ankiwrap, sql_cards):
    """ Test how cards are found. """

    with pytest.raises(click.exceptions.Abort):
        cutils.find_cards(ankiwrap.col, ids=False, flt="; DROP ALL TABLES; --", raw=False, qry=None)

    anki_cards = cutils.find_cards(ankiwrap.col, ids=False, flt="where id = 0", raw=False, qry=None)
    assert len(anki_cards) == 0

    anki_cards_ids = cutils.find_cards(ankiwrap.col, ids=True, flt=None, raw=False, qry=None)
    assert len(anki_cards_ids) == len(sql_cards)
    for card_id in anki_cards_ids:
        assert card_id in [card['id'] for card in sql_cards]

    anki_cards = cutils.find_cards(ankiwrap.col, ids=False, flt=None, raw=False, qry=None)
    check_anki_cards_content(anki_cards, sql_cards)

    anki_cards = cutils.find_cards(ankiwrap.col, ids=False, flt="", raw=False, qry=None)
    check_anki_cards_content(anki_cards, sql_cards)

    raw_cards = cutils.find_cards(ankiwrap.col, ids=False, flt=None, raw=True, qry=None)
    check_raw_cards_content(raw_cards, sql_cards)

    anki_cards = cutils.find_cards(ankiwrap.col, ids=False, flt=None, raw=False, qry="*")
    check_anki_cards_content(anki_cards, sql_cards)
