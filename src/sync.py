"""
Anki-cli sync manager.
"""

# LEGAL REQUIREMENTS
####################
# (cf. https://gitlab.com/stephane.tzvetkov/anki-cli#legal-requirements for more details)
#
# ORIGINAL FILE: https://github.com/ankitects/anki/blob/2.1.16/aqt/sync.py
# (One can also find the original file in the anki-cli project, in the git submodule:
# "anki-2_1_16-submodule/aqt/sync.py")
#
# ORIGINAL FILE COPYRIGHT: "Ankitects Pty Ltd and contributors"
#
# ORIGINAL FILE LICENSE: "GNU AGPL, version 3 or later; http://www.gnu.org/licenses/agpl.html"
# Cf. https://github.com/ankitects/anki/blob/2.1.16/LICENSE

import sys
import time
import traceback

import click

from anki.storage import Collection
from anki.hooks import addHook, remHook
from anki.lang import _
from anki.sync import Syncer, RemoteServer, FullSyncer, MediaSyncer, RemoteMediaServer


# Sync manager
######################################################################

class SyncManager():
    """ Interface to anki.sync in order to manage syncs. """

    def __init__(self, pm, cancel=None, download=None, upload=None):
        self.pm = pm
        self.auto_cancel = cancel
        self.auto_download = download
        self.auto_upload = upload

    def sync(self, auth=None):
        """ Main sync function """

        self.path = self.pm.collectionPath()
        self.hkey = self.pm.profile['syncKey']
        self.auth = auth
        self.media = self.pm.profile['syncMedia']
        self.hostNum = self.pm.profile.get("hostNum")
        self._abort = 0 # 1=flagged, 2=aborting
        #t.event.connect(self.onEvent)
        click.echo("\nConnecting...\n")
        self.label = _("Connecting...")

        #prog = self.mw.progress.start(immediate=True, label=self.label)
        self.sentBytes = self.recvBytes = 0

        self.run()
        #self.mw.progress.finish()
        if self.syncMsg:
            click.echo(self.syncMsg)
        if self.uname:
            self.pm.profile['syncUser'] = self.uname
        self.pm.profile['hostNum'] = self.hostNum

        def delayedInfo():
            if self._didFullUp and not self._didError:
                click.echo("Your collection was successfully uploaded to AnkiWeb. \
                             \
                            If you use any other devices, please sync them now, and choose \
                            to download the collection you have just uploaded from this computer. \
                            After doing so, future reviews and added cards will be merged \
                            automatically.")

    def onEvent(self, evt, *args):
        click.echo(f"\nSync Event: {evt}\n")
        if evt == "badAuth":
            click.secho("\nError:\nIncorrect AnkiWeb ID or password; please try again.\n"
                        +"Or too many logins to your account; please try again in an hour.\n"
                        , fg="red")
            # blank the key so we prompt user again:
            self.pm.profile['syncKey'] = None
            self.pm.save()
            raise click.Abort()
        elif evt == "corrupt":
            pass
        elif evt == "newKey":
            self.pm.profile['syncKey'] = args[0]
            self.pm.save()
        elif evt == "offline":
            click.echo("Syncing failed; internet offline.")
        elif evt == "upbad":
            self._didFullUp = False
            self._checkFailed()
        elif evt == "sync":
            m = None; t = args[0]
            if t == "login":
                m = _("Syncing...")
            elif t == "upload":
                self._didFullUp = True
                m = _("Uploading to AnkiWeb...")
            elif t == "download":
                m = _("Downloading from AnkiWeb...")
            elif t == "sanity":
                m = _("Checking...")
            elif t == "findMedia":
                m = _("Checking media...")
            elif t == "upgradeRequired":
                click.echo("Please visit AnkiWeb, upgrade your deck, then try again.")
            if m:
                self.label = m
        elif evt == "syncMsg":
            self.label = args[0]
        elif evt == "error":
            self._didError = True
            click.echo(f"Syncing failed:\n{self._rewriteError(args[0])}")
        elif evt == "clockOff":
            self._clockOff()
        elif evt == "checkFailed":
            self._checkFailed()
        elif evt == "mediaSanity":
            click.echo("Warning: \
                    A problem occurred while syncing media. Please use Tools>Check Media, then \
                    sync again to correct the issue.")
        elif evt == "noChanges":
            pass
        elif evt == "fullSync":
            self._confirmFullSync()
        elif evt == "downloadClobber":
            click.echo("Your AnkiWeb collection does not contain any cards. \
                        Please sync again and choose 'Upload' instead.")
        elif evt == "send":
            # posted events not guaranteed to arrive in order
            self.sentBytes = max(self.sentBytes, int(args[0]))
        elif evt == "recv":
            self.recvBytes = max(self.recvBytes, int(args[0]))

    def _rewriteError(self, err):
        if "Errno 61" in err:
            return _(
                "Couldn't connect to AnkiWeb. "
                +"Please check your network connection and try again."
                )
        if "timed out" in err or "10060" in err:
            return _(
                "The connection to AnkiWeb timed out. "
                +"Please check your network connection and try again."
                )
        if "code: 500" in err:
            return _(
                "AnkiWeb encountered an error. Please try again in a few minutes, "
                +"and if the problem persists, please file a bug report."
                )
        if "code: 501" in err:
            return _(
                "Please upgrade to the latest version of Anki."
                )
        if "code: 502" in err:
        # 502 is technically due to the server restarting, but we reuse the error message:
            return _(
                "AnkiWeb is under maintenance. Please try again in a few minutes."
                )
        if "code: 503" in err:
            return _(
                "AnkiWeb is too busy at the moment. Please try again in a few minutes."
                )
        if "code: 504" in err:
            return _(
                "504 gateway timeout error received. "
                +"Please try temporarily disabling your antivirus."
                )
        if "code: 409" in err:
            return _(
                "Only one client can access AnkiWeb at a time. "
                +"If a previous sync failed, please try again in a few minutes."
                )
        if "10061" in err or "10013" in err or "10053" in err:
            return _(
                "Antivirus or firewall software is preventing Anki from connecting to the internet."
                )
        if "10054" in err or "Broken pipe" in err:
            return _(
                "Connection timed out. Either your internet connection is experiencing problems, "
                +"or you have a very large file in your media folder."
                )
        if "Unable to find the server" in err or "socket.gaierror" in err:
            return _(
                "Server not found. Either your internet connection is down, "
                +"or antivirus/firewall software is blocking Anki from connecting to the internet."
                )
        if "code: 407" in err:
            return _(
                "Proxy authentication required."
                )
        if "code: 413" in err:
            return _(
                "Your collection or a media file is too large to sync."
                )
        if "EOF occurred in violation of protocol" in err:
            return _(
                "Error establishing a secure connection. This is usually caused by antivirus,"
                +"firewall or VPN software, or problems with your ISP."
                ) + " (eof)"
        if "certificate verify failed" in err:
            return _(
                "Error establishing a secure connection. This is usually caused by antivirus,"
                +"firewall or VPN software, or problems with your ISP."
                ) + " (invalid cert)"
        return err

    def _confirmFullSync(self):

        if self.auto_cancel:
            self.fullSyncChoice = "cancel"
            return
        elif self.auto_download:
            self.fullSyncChoice = "download"
            return
        
        else:
            diag = 0
            if self.localIsEmpty:
                diag = click.prompt('\
                        Local collection has no cards.\n \
                        * Cancel? (enter 0)\n \
                        * Download from AnkiWeb? (enter 1)\n \
                        ', type=int)
            else:
                if self.auto_upload:
                    self.fullSyncChoice = "upload"
                    return

                diag = click.prompt("\
                        Your decks here and on AnkiWeb differ in such a way that they can't\n \
                        be merged together, so it's necessary to overwrite the decks on one\n \
                        side with the decks from the other.\n \
                        \n \
                        If you choose download, Anki will download the collection from AnkiWeb,\n \
                        and any changes you have made on your computer since the last sync will\n \
                        be lost.\b \
                        \n \
                        If you choose upload, Anki will upload your collection to AnkiWeb, and\n \
                        any changes you have made on AnkiWeb or your other devices since the\n \
                        last sync to this device will be lost.\n \
                        \n \
                        After all devices are in sync, future reviews and added cards can be merged\n \
                        automatically.\n \
                        \n \
                        * Cancel? (enter 0)\n \
                        * Download from AnkiWeb? (enter 1)\n \
                        * Upload to AnkiWeb? (enter 2)\n \
                        ", type=int)

            if diag == 2:
                self.fullSyncChoice = "upload"
            elif diag == 1:
                self.fullSyncChoice = "download"
            else:
                self.fullSyncChoice = "cancel"

    def _clockOff(self):
        click.echo("Syncing requires the clock on your computer to be set correctly. Please \
                    fix the clock and try again.")

    def _checkFailed(self):
        click.echo("Your collection is in an inconsistent state. Please run Tools>\
                    Check Database, then sync again.")

    def run(self):
        # init this first so an early crash doesn't cause an error
        # in the main thread
        self.syncMsg = ""
        self.uname = ""
        try:
            self.col = Collection(self.path, log=True)
        except:
            #tb = traceback.format_exc()
            #print(tb)
            self.onEvent("corrupt")
            return
        self.server = RemoteServer(self.hkey, hostNum=self.hostNum)
        self.client = Syncer(self.col, self.server)
        self.sentTotal = 0
        self.recvTotal = 0
        def syncEvent(type):
            self.onEvent("sync", type)
        def syncMsg(msg):
            self.onEvent("syncMsg", msg)
        def sendEvent(bytes):
            if not self._abort:
                self.sentTotal += bytes
                self.onEvent("send", str(self.sentTotal))
            elif self._abort == 1:
                self._abort = 2
                raise Exception("sync cancelled")
        def recvEvent(bytes):
            if not self._abort:
                self.recvTotal += bytes
                self.onEvent("recv", str(self.recvTotal))
            elif self._abort == 1:
                self._abort = 2
                raise Exception("sync cancelled")
        addHook("sync", syncEvent)
        addHook("syncMsg", syncMsg)
        addHook("httpSend", sendEvent)
        addHook("httpRecv", recvEvent)
        # run sync and catch any errors
        try:
            self._sync()
        except Exception as e:
            err = repr(str(e))
            click.echo(f"Error: {err}")
            #err = traceback.format_exc()
            self.onEvent("error", err)
        finally:
            # don't bump mod time unless we explicitly save
            #self.col.close(save=False) # ? this line should not be commented... ?
            remHook("sync", syncEvent)
            remHook("syncMsg", syncMsg)
            remHook("httpSend", sendEvent)
            remHook("httpRecv", recvEvent)

    def _abortingSync(self):
        try:
            return self.client.sync()
        except Exception as e:
            if "sync cancelled" in str(e):
                self.server.abort()
                raise
            else:
                raise

    def _sync(self):
        if self.auth:
            # need to authenticate and obtain host key
            self.hkey = self.server.hostKey(*self.auth)
            if not self.hkey:
                # provided details were invalid
                return self.onEvent("badAuth")
            else:
                # write new details and tell calling thread to save
                self.onEvent("newKey", self.hkey)
        # run sync and check state
        try:
            ret = self._abortingSync()
        except Exception as e:
            #log = traceback.format_exc()
            err = repr(str(e))
            if ("Unable to find the server" in err or
                "Errno 2" in err or "getaddrinfo" in err):
                self.onEvent("offline")
            elif "sync cancelled" in err:
                pass
            else:
                self.onEvent("error", err)
            return
        if ret == "badAuth":
            return self.onEvent("badAuth")
        elif ret == "clockOff":
            return self.onEvent("clockOff")
        elif ret == "basicCheckFailed" or ret == "sanityCheckFailed":
            return self.onEvent("checkFailed")
        # full sync?
        if ret == "fullSync":
            return self._fullSync()
        # save and note success state
        if ret == "noChanges":
            self.onEvent("noChanges")
        elif ret == "success":
            self.onEvent("success")
        elif ret == "serverAbort":
            self.syncMsg = self.client.syncMsg
            return
        else:
            self.onEvent("error", "Unknown sync return code.")
        self.syncMsg = self.client.syncMsg
        self.uname = self.client.uname
        self.hostNum = self.client.hostNum
        # then move on to media sync
        self._syncMedia()

    def _fullSync(self):
        # tell the calling thread we need a decision on sync direction, and
        # wait for a reply
        self.fullSyncChoice = False
        self.localIsEmpty = self.col.isEmpty()
        self.onEvent("fullSync")
        while not self.fullSyncChoice:
            time.sleep(0.1)
        f = self.fullSyncChoice
        if f == "cancel":
            return
        self.client = FullSyncer(self.col, self.hkey, self.server.client,
                                 hostNum=self.hostNum)
        try:
            if f == "upload":
                if not self.client.upload():
                    self.onEvent("upbad")
            else:
                ret = self.client.download()
                if ret == "downloadClobber":
                    self.onEvent(ret)
                    return
        except Exception as e:
            if "sync cancelled" in str(e):
                return
            raise
        # reopen db and move on to media sync
        self.col.reopen()
        self._syncMedia()

    def _syncMedia(self):
        if not self.media:
            return
        self.server = RemoteMediaServer(self.col, self.hkey, self.server.client,
                                        hostNum=self.hostNum)
        self.client = MediaSyncer(self.col, self.server)
        try:
            ret = self.client.sync()
        except Exception as e:
            if "sync cancelled" in str(e):
                return
            raise
        if ret == "noChanges":
            self.onEvent("noMediaChanges")
        elif ret == "sanityCheckFailed" or ret == "corruptMediaDB":
            self.onEvent("mediaSanity")
        else:
            self.onEvent("mediaSuccess")
