"""
Notes utility functions for the Anki-wrapper.
"""

import json
import os
import sys
import tempfile
import warnings

from subprocess import call
from pprint import pprint
import click

from anki.notes import Note
from anki.utils import intTime
import src.ankiwrap
import src.cards_utils as cutils

# TODO reorder functions in a more logical way
# TODO create associated anki-cli commands for the functions that still don't have one
# TODO check default mid ('Basic'), default did ('Default'), and other default values existence
# TODO add confirmation to update command (skip with -y --yes)
# TODO make sure that usn is set to -1 when creating or updating a note, or for any modification 
#      (to indicate changes to be synced with the server)

# TODO docstring every public functions like this one:
def find_notes(col, ids=False, flt=None, raw=False, qry=None, vrb=False, slt=False):
    """
    Find filtered note(s).

    This is a reST style.
    cf. https://stackoverflow.com/questions/3898572/what-is-the-standard-python-docstring-format

    :param param1 (string):     this is a first param
    :param param2 (boolean):    this is a second param

    :returns (string): this is a description of what is returned

    :raises keyError:   raises an exception
    :raises otherError: raises an exception
    """
    if flt:
        if not isinstance(flt, str):
            click.secho("Your filter should be a string!", fg="red")
            raise click.Abort()

        if not flt.find(';'):
            click.secho("If you want to execute an sql command, "
                        +"please do not try to insert one here, "
                        +"use the 'execute' command instead! "
                        +"Run `$ anki-cli execute -h` for more details.", fg="red")
            raise click.Abort()
    else:
        flt = ""

    if ids:
        query = f"select id from notes {flt}"
        if vrb and not slt:
            click.echo(f"\nNote(s) ids found with the following query: `{query}`\n")
        results = col.db.list(query)
        if not slt:
            pprint(results)
            click.echo("\n")
        return results

    elif qry:
        if isinstance(qry, str) is False:
            click.secho("Your anki-query should be a string!", fg="red")
            raise click.Abort()
        results = col.findNotes(qry)
        if vrb and not slt:
            click.echo(f"\nAnki-note(s) found with the following Anki embedded query: `{qry}`\n")
        notes_list = []
        for i in results:
            note = col.getNote(i)
            notes_list.append(note)
            if not slt:
                print_note(note)
        return notes_list

    elif raw:
        query = f"select * from notes {flt}"
        if vrb and not slt:
            click.echo(f"\nRaw DB note(s) found with the following query: `{query}`\n")
        results = col.db.execute(query)
        results_list = []
        for i in results:
            results_list.append(i)
            if not slt:
                click.echo("note:")
                pprint(i)
                click.echo("\n")
        return results_list

    else:
        query = f"select id from notes {flt}"
        if vrb and not slt:
            click.echo(f"\nAnki-note(s) found with the following query: `{query}`\n")
        results = col.db.list(query)
        notes_list = []
        for i in results:
            note = col.getNote(i)
            notes_list.append(note)
            if not slt:
                print_note(note)
        return notes_list

def print_note(note):
    """
    Print a note.
    """
    cid = [str(c.id) for c in note.cards()]
    click.echo(f"note id: {note.id}")
    click.echo(f"{{'id': {str(note.id)},\n"
               f" 'guid': {str(note.guid)},\n"
               f" 'mid': {str(note.mid)},\n"
               f" 'mod': {str(note.mod)},\n"
               f" 'usn': {str(note.usn)},\n"
               f" 'tags': {str(note.tags)},\n"
               f" 'flds': {str(note.fields)},\n"
               # missing sfld
               # missing csum
               f" 'flags': {str(note.flags)},\n"
               f" 'data': {str(note.data)},\n"
               f" '[additional] cid': {str(cid)} }}\n")

def add_note(col, flds, nid=None, mid=None, did=None, tags=None, vrb=False):
    """
    Create a note.
    """
    if vrb:
        click.echo("\nVerbose:\n\t- creating note...")

    if mid:
        model = col.models.get(mid)
        if not model:
            click.secho(f"\nError:\nNote creation aborted! This model id cannot be found: '{mid}'!"
                        , fg="red")
            raise click.Abort()
    else:
        model = col.models.byName('Basic')
        if not model:
            click.secho("\nError:\nNote creation aborted! The 'Basic' model cannot be found, it "
                        "has probably been deleted, meaning no default mid can be used! In this "
                        "case the mid has to be specified.", fg="red")
            raise click.Abort()
        if vrb:
            click.echo("\t- note's model id will be set to the 'Basic' model (default).")

    note = Note(col=col, model=model)
    note.flush(mod=intTime())
    if vrb:
        click.echo(f"\t- note's model id set to '{note.mid}'.")

    if nid:
        set_note_nid(col=col, note=note, new_nid=nid)
        if vrb:
            click.echo(f"\t- note's id set to '{nid}'.")
    elif vrb:
        click.echo(f"\t- note's id set to '{note.id}' (by default).")

    if did:
        set_note_model_did(col, note, did)
        if vrb:
            click.echo(f"\t- note's model deck id set to '{did}'.")
    else:
        did = 1 # 'Default' deck id
        if vrb:
            click.echo("\t- note's model deck id set to '1' ('Default' deck).")

    if flds:
        set_note_flds(col=col, note=note, flds=flds, flds_are_needed=True)
        if vrb:
            click.echo(f"\t- note's fields set to '{flds}'.")

    if tags:
        set_note_tags(note=note, tags=tags)
        if vrb:
            click.echo(f"\t- note's tags set to '{tags}'.")

    if len(find_notes(col=col, ids=True, flt=f"where id = {note.id}", slt=True)) < 2:
        col.addNote(note)
        src.ankiwrap.modif_should_be_synced = True
        if vrb:
            click.echo(f"\t- Note '{note.id}' created.") # TODO echo in bold text
        return note
    else:
        click.secho(f"\nError:\nNote '{note.id}' not created: duplicated note id detected!"
                    , fg="red")
        raise click.Abort()

def del_notes(col, ids, also_del_cards=True, vrb=False, slt=False):
    """
    Delete notes and eventually their associated cards.
    """
    if not ids or not isinstance(ids, list) or (isinstance(ids, list) and len(ids) == 0):
        if not slt:
            warnings.warn("")
            click.secho("\nWarning:\nNo id has been given, nothing will be deleted.", fg="yellow")
    else:
        for i in ids:
            if len(find_notes(col=col, flt=f"where id = {i}", slt=True)) != 0:
                if also_del_cards:
                    if not col.getNote(i).cards():
                        col._remNotes([i])
                        if vrb:
                            click.secho(f"\nVerbose:\n\t- Note '{i}' has been deleted (no "
                                        f" associated card found to be deleted).", fg="green")
                    else:
                        col.remNotes([i])
                        if vrb:
                            click.secho(f"\nVerbose:\n\t- Note '{i}' and it's associated card(s) "
                                        f"has been deleted.", fg="green")
                else:
                    note_cards = col.getNote(i).cards()
                    col._remNotes([i])
                    if note_cards and not slt:
                        warnings.warn("")
                        click.secho(f"\nWarning:\n"
                                    f"You have deleted note '{i}' without deleting its associated "
                                    f"cards: 'note_cards'. Afterward, make sure to "
                                    f"delete those cards, or to bind the cards to other notes "
                                    f"(through the nid field of the cards).\n", fg="yellow")
                    elif vrb:
                        click.secho(f"\nVerbose:\n\t- Note '{i}' has been deleted (no associated "
                                    f"card found to be deleted).", fg="green")
                src.ankiwrap.modif_should_be_synced = True
            else:
                click.secho(f"\nError:\n"
                            f"Note not deleted. This note id can't be found: {i}", fg="red")
                raise click.Abort()

def upd_note(col, nid, new_nid=None, mid=None, did=None, tags=None, flds=None, vrb=False):
    """
    Update a note (without altering its card statistics).
    """
    if vrb:
        click.echo(f"\nVerbose:\n\t- updating note '{nid}'...")

    if col.getNote(nid):
        note = Note(col=col, id=nid)
    else:
        click.secho(f"\nError:\n"
                    f"No note has been updated. Because no note with this id does exists: '{nid}'!"
                    , fg="red")
        raise click.Abort()

    if new_nid:
        set_note_nid(col=col, note=note, new_nid=new_nid)
        if vrb:
            click.echo(f"\t- The note id of the note '{note.id}' has been updated to '{new_nid}'.")
    if mid:
        set_note_mid(col=col, note=note, mid=mid)
        if vrb:
            click.echo(f"\t- The model id of the note '{note.id}' has been updated to "
                       f"'{note.mid}'.")
    if did:
        set_note_model_did(col=col, note=note, did=did)
        if vrb:
            click.echo(f"\t- The model did of the note '{note.id}' has been updated to "
                       f"'{note.model()['id']}'.")
    if tags:
        set_note_tags(note=note, tags=tags)
        if vrb:
            click.echo(f"\t- Tags of note '{note.id}' has been updated to '{note.tags}'.")
    if flds:
        set_note_flds(col=col, note=note, flds=flds, flds_are_needed=True)
        if vrb:
            click.echo(f"\t- Fields of note '{note.id}' has been updated to '{note.fields}'.")
    col.db.commit()
    return note

def update_note_with_editor(note_id):
    """
    Update a note, without altering it's statistics, with the default editor.
    """
    #TODO: tests

    tmp_file_content = "plop"
    with tempfile.NamedTemporaryFile(mode='w+',
                                     dir=os.getcwd(),
                                     prefix='note_',
                                     suffix='.md',
                                     delete=False) as tmp_file:
        tmp_file.write(tmp_file_content)
        tmp_file.flush()
        retcode = call([os.environ.get('EDITOR', 'vim'), tmp_file.name])

        if retcode != 0:
            click.secho(f"Editor return with exit code: {retcode}", fg="red")
            return []
    # TODO

def set_note_nid(col, note, new_nid, vrb=False):
    """
    Set the id of a given note.
    """
    if new_nid and isinstance(new_nid, int) and new_nid > 0:
        if not find_notes(col=col, ids=True, flt=f"where id = {new_nid}", slt=True):
            cards = note.cards()
            old_nid = note.id
            note.id = new_nid
            note.flush(mod=intTime())
            # Updating with a new nid is like creating a new one, so the previous one shall be del:
            del_notes(col=col, ids=[old_nid], also_del_cards=False, slt=True)
            # associated cards (if any) should also be updated:
            for card in cards:
                card.nid = new_nid
                card.note(reload=True)
                card.flush()
            src.ankiwrap.modif_should_be_synced = True
            if vrb:
                click.echo(f"\nVerbose:\n\t- The note id of the note '{old_nid}' has been set to "
                           f"'{new_nid}'.")
        else:
            click.secho(f"\nError:\n"
                        f"The note id of the note '{note.id}' has not been set to '{new_nid}', "
                        f"because a note with the id '{new_nid}' already exists!", fg="red")
            raise click.Abort()
    else:
        click.secho(f"\nError:\n"
                    f"The note id of the note '{note.id}' cannot been set to '{new_nid}' "
                    f"(either because it's not an integer or because it is a negative integer)!"
                    , fg="red")
        raise click.Abort()

def set_note_mid(col, note, mid, vrb=False):
    """
    Set the model id of a given note.
    """
    if col.models.get(mid):
        note.mid = mid
        note.flush(mod=intTime())
        src.ankiwrap.modif_should_be_synced = True
        if vrb:
            click.echo(f"\nVerbose:\n\t- The model id of the note '{note.id}' has been set to "
                       f"'{mid}'.")
    else:
        click.secho(f"\nError:\nNote's mid not set. This model id cannot be found: '{mid}'!"
                    , fg="red")
        raise click.Abort()

def set_note_model_did(col, note, did, vrb=False):
    """
    Set the deck id of a given note's model.
    """
    if str(did) in col.decks.decks:
        deck = col.decks.decks[str(did)]
        model = col.models.get(note.model()['id'])
        model['did'] = deck['id']
        col.models.save(model)
        col.models.flush()
        src.ankiwrap.modif_should_be_synced = True
        if vrb:
            click.echo(f"\nVerbose:\n\t- The deck id of the model '{model['name']}' (with model id "
                       f"'{model['id']}') has been set to '{did}' (the note '{note.id}' is "
                       f"associated to this model).")
    else:
        click.secho(f"\nError:\n"
                    f"Note's model deck id not set. This did cannot be found: '{did}'", fg="red")
        raise click.Abort()

def set_note_flds(col, note, flds, flds_are_needed=False, vrb=False, slt=False):
    """
    Set the field(s) of a given note.
    """
    if flds:
        if not flds[0]:
            click.secho(f"\nError:\nAt least first field of the note '{note.id}' should not be "
                        f"empty!", fg="red")
            raise click.Abort()
        #anki_note.fields = [convert_to_html(f) for f in fields]
        model = col.models.get(note.mid)
        note.fields = flds
        check_flds(model, note)
        note.flush(mod=intTime())
        src.ankiwrap.modif_should_be_synced = True
        if vrb:
            click.echo(f"\nVerbose:\n\t- Field(s) {flds} have been assigned to note '{note.id}'.")
    elif flds_are_needed:
        click.secho(f"\nError:\nNo field(s) have been passed to be assigned to note '{note.id}'!"
                    , fg="red")
        raise click.Abort()
    elif not slt:
        warnings.warn("")
        click.secho(f"\nWarning:\nNo field(s) have been passed to be assigned to note '{note.id}'!"
                    , fg="yellow")

def _parse_tags(tags):
    """
    Return "sub tags" of a tab list. I.e. Anki uses the space (' ') character as a tag separator, so
    in a tags list, if an item contains multiple space separated words: this function will return a
    list of each word of all items.
    """
    if not isinstance(tags, list):
        click.secho(f"\nError:\nTags to parse need to be in a list, or wrong result may be "
                    f"returned!, ", fg="red")
        raise click.Abort()
    sub_tags = []
    for tag in tags:
        for sub_tag in tag.replace('\u3000', ' ').split(" "):
            if sub_tag:
                sub_tags.append(sub_tag)
    return sub_tags

def rip_note_tags(note, vrb=False):
    """
    Delete all tags of a given note.
    """
    note.tags.clear()
    note.flush(mod=intTime())
    src.ankiwrap.modif_should_be_synced = True
    if vrb:
        click.echo(f"\nVerbose:\n\t- All tags of note '{note.id}' have been deleted!")

def set_note_tags(note, tags, vrb=False):
    """
    Set the tag(s) of a given note. NOTE: Anki uses the space (' ') character as a tag separator.
    The 'tags' argument being a string list, if a 'tags' item contains two words separated by a
    space: it will result in two different tags, one for each word.
    """
    if tags:
        rip_note_tags(note) # wipe any pre-existing tags
        if vrb:
            click.echo(f"\nVerbose:\n\t- old tags from note '{note.id}' have been deleted and new "
                       f" ones will be added.")
        add_note_tags(note, tags, vrb)
    elif vrb:
        click.echo(f"\nVerbose:\n\t- No tags have been passed to be assigned to note '{note.id}'.")

def add_note_tags(note, tags, vrb=False, slt=False):
    """
    Add tag(s) to a given note.
    """
    if tags:
        if not isinstance(tags, list):
            click.secho(f"\nError:\nTags to be added needs to be in a list, or wrong result may be "
                        f"returned!, ", fg="red")
            raise click.Abort()
        for tag in tags:
            tag_has_been_added = False
            if note.hasTag(tag):
                if not slt:
                    warnings.warn("")
                    click.secho(f"\nWarning:\n"
                                f"The tag '{tag}' is already present in note '{note.id}' and won't "
                                f"be added!", fg="yellow")
                break
            note.addTag(tag)
            tag_has_been_added = True
        if tag_has_been_added:
            note.flush(mod=intTime())
            src.ankiwrap.modif_should_be_synced = True
    if vrb:
        if not tags:
            click.echo(f"\nVerbose:\n\t- No tags have been passed to be assigned to note "
                       f"'{note.id}'.")
        else:
            for tag in _parse_tags(tags):
                click.echo(f"\nVerbose:\n\t-  Tag '{tag}' added for note '{note.id}'.")

def del_note_tags(note, tags, vrb=False, slt=False):
    """
    Remove tag(s) from a given note.
    """
    if tags:
        to_remove = []
        tags = _parse_tags(tags)
        for tag in tags:
            for note_tag in note.tags:
                if note_tag.lower() == tag.lower():
                    to_remove.append(tag)
        for tag in to_remove:
            note.tags.remove(tag)
            note.flush(mod=intTime())
            src.ankiwrap.modif_should_be_synced = True
        if vrb:
            click.echo(f"\nVerbose:\n\t-  Tags {to_remove} have been deleted from note "
                       f"'{note.id}'.")
        if not slt and len(list(set(tags) - set(to_remove))) > 0:
            warnings.warn("")
            click.secho(f"\nWarning:\n"
                        f"Tags {list(set(tags) - set(to_remove))} were not found in note "
                        f"'{note.id}' and have not been deleted.", fg="yellow")
    elif not slt:
        warnings.warn("")
        click.secho(f"\nWarning:\n"
                    f"No tags have been specified to be removed from note '{note.id}'.",
                    fg="yellow")

def check_flds(model, note):
    """
    Check that the fields "shared" between the note and the field are consistent.
    """
    model_field_names = [field['name'] for field in model['flds']]

    field_names = note.keys()
    field_values = note.fields

    if len(field_names) != len(model_field_names):
        click.secho(f"\nError:\n"
                    f"Not as many fields names in note '{note.id}' "
                    f"as in model '{model['id']}'!"
                    f"\n\nField(s) cannot be set.", fg="red")
        raise click.Abort()

    if len(field_values) != len(model_field_names):
        click.secho(f"\nError:\n"
                    f"Not as many fields values in note '{note.id}' "
                    f"as in model '{model['id']}'!"
                    f"\n\nField(s) cannot be set.", fg="red")
        raise click.Abort()

    if len(field_names) != len(field_values):
        click.secho(f"\nError:\n"
                    f"Not as many fields names as fields values in note '{note.id}'!"
                    f"\n\nField(s) cannot be set.", fg="red")
        raise click.Abort()

    for i, j in zip(model_field_names, field_names):
        if i != j:
            click.secho(
                f"\nError:\n"
                f"The the note's field name '{i}' (note: '{note.id}') is inconsistent "
                f"compared to the associated model's field name '{j}' (model: '{model['id']}')."
                f"\n\nField(s) cannot be set."
                , fg="red")
            raise click.Abort()
