"""
Import the official anki lib (without qt), from the anki submodule.
"""

import os
import sys

from pathlib import Path
ANKI_CLI_DIR = Path(os.path.realpath(__file__)).parent.parent
ANKI_LIB_DIR = Path(str(ANKI_CLI_DIR)+"/anki-2_1_16-submodule")

sys.path.append(str(ANKI_CLI_DIR))
sys.path.append(str(ANKI_LIB_DIR))
