"""
Sync utility functions for the Anki-wrapper.
"""
import time

import click

from anki.storage import Collection
from anki.sync import Syncer, RemoteServer, FullSyncer, MediaSyncer, RemoteMediaServer

def sync(pm, auth=None):
    """
    .
    """
    if not pm:
        click.secho("\nError:\nNo profile available!", fg="red")
        click.Abort()
    path = pm.collectionPath()

    #if not pm.profile['syncKey']:
    #    click.secho("\nError:\nNo credentials registered!", fg="red")
    #    click.Abort()
    hkey = pm.profile['syncKey']

    if not pm.profile['syncMedia']:
        click.secho("\nError:\nNo media available!", fg="red")
        click.Abort()
    media = pm.profile['syncMedia']

    host_num = pm.profile.get("hostNum")

    try:
        col = Collection(path, log=True)
    except Exception as e:
        print(f">>>error:\n {e}")

    server = RemoteServer(hkey, hostNum=host_num)
    client = Syncer(col, server)

    try:
        ret = client.sync()
    except Exception as e:
        if "sync cancelled" in str(e):
            server.abort()
        click.secho('\nError during sync:\n\n{e}', fg='red')
        raise click.Abort()

    from pprint import pprint
    pprint(ret)

    if ret == "badAuth":
        #TODO
        print(f">>> {ret}")
        pass
    elif ret == "clockOff":
        #TODO
        print(f">>> {ret}")
        pass
    elif ret in ["basicCheckFailed", "sanityCheckFailed"]:
        #TODO
        print(f">>> {ret}")
        pass
    if ret == "fullSync":
        full_sync(col, media, client, server, hkey, host_num)
    if ret == "noChanges":
        #TODO
        print(f">>> {ret}")
        pass
    elif ret == "success":
        #TODO
        print(f">>> {ret}")
        pass
    elif ret == "serverAbort":
        #TODO
        print(f">>> {ret}")
        pass
    else:
        click.secho(f"\nError:\n Unknown sync return code: '{ret}'", fg="red")
        raise click.Abort()

    sync_msg = client.syncMsg
    print(f">>> sync msg: {sync_msg}")

    uname = client.uname
    host_num = client.hostNum

    # then move on to media sync:
    sync_media(col, media, client, server, hkey, host_num)

def full_sync_choice(is_local_empty):
    """
    .
    """
    #if auto_cancel:
    #    fullSyncChoice = "cancel"
    #    return
    #elif auto_download:
    #    fullSyncChoice = "download"
    #    return

    #else:
    diag = 0
    if is_local_empty:
        diag = click.prompt(
            f"Local collection has no cards.\n"
            f"* Cancel? (enter '0')\n"
            f"* Download from AnkiWeb? (enter '1')\n"
            , type=int)
    else:
        #if auto_upload:
        #    fullSyncChoice = "upload"
        #    return

        diag = click.prompt(
            f"Your collections, the local one on your current computer and the remote one on\n"
            f"AnkiWeb, differ in such a way that they can't be merged together, so it's\n"
            f"necessary to overwrite the collection on one side with the collection from the\n"
            f"other side.\n"
            f"\n"
            f"If you choose \"Download from AnkiWeb\", anki-cli will download your remote\n"
            f"collection from AnkiWeb and overwrite your local collection: any changes you\n"
            f"have made on your computer since the last sync will be lost.\n"
            f"\n"
            f"If you choose \"Upload to AnkiWeb\", Anki will upload your collection to AnkiWeb,\n"
            f"and any changes you have made on AnkiWeb or your other devices since the last sync\n"
            f"to this device will be lost.\n"
            f"\n"
            f"After all devices are in sync, future reviews and added cards can be merged\n"
            f"automatically.\n"
            f"\n"
            f"* Cancel? (enter 0)\n"
            f"* Download from AnkiWeb? (enter 1)\n"
            f"* Upload to AnkiWeb? (enter 2)\n"
            , type=int)

    if diag == 2:
        return "upload"
    elif diag == 1:
        return "download"
    else:
        return "cancel"


def full_sync(col, media, client, server, hkey, host_num):
    """
    .
    """
    is_local_empty = col.isEmpty()
    choice = full_sync_choice(is_local_empty)
    if choice == "cancel":
        print(">>>cancel")
        return
    client = FullSyncer(col, hkey, server.client, hostNum=host_num)
    try:
        if choice == "upload":
            if not client.upload():
                #TODO
                pass
        else:
            ret = client.download()
            print(f">>>client download return: {ret}")
            if ret == "downloadClobber":
                print(">>> return after: downloadClobber")
                return
    except Exception as e:
        print(f"\nError:\n{e}\n")
        if "sync cancelled" in str(e):
            print(">>> sync cancelled")
            return
        raise

    # reopen db and move on to media sync
    col.reopen()
    sync_media(col, media, client, server, hkey, host_num)

def sync_media(col, media, client, server, hkey, host_num):
    """
    .
    """
    if not media:
        click.echo(">>>no media!")
        return
    server = RemoteMediaServer(col, hkey, server.client, hostNum=host_num)
    client = MediaSyncer(col, server)
    try:
        ret = client.sync()
    except Exception as e:
        if "sync cancelled" in str(e):
            return
        raise
    if ret == "noChanges":
        #TODO
        pass
    elif ret == "sanityCheckFailed" or ret == "corruptMediaDB":
        #TODO
        pass
    else:
        #TODO
        # "mediaSuccess"
        pass
